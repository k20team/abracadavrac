var Encore = require('@symfony/webpack-encore');

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath('public/dist/')
    .setPublicPath('/dist')
    .cleanupOutputBeforeBuild()

    // ADD ENTRY
    .addEntry('js/theme', './templates/assets/js/main.js')
    //.addEntry('js/stripe', './templates/assets/js/stripe.js')
    /*.addEntry('js/login', './assets/js/login.js')
    .addEntry('js/admin', './assets/js/admin.js')
    .addEntry('js/search', './assets/js/search.js')*/
    .addStyleEntry('css/theme', ['./templates/assets/scss/main.scss'])
    //.addStyleEntry('css/admin', ['./assets/scss/admin.scss'])

    .enableSassLoader(()=>{},{
        // Fix windows build resolve URL
        resolveUrlLoader: {
            linefeed: 'crlf'
        }
    })
    .enableVueLoader()

    .enablePostCssLoader()

    //.enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())

    .autoProvidejQuery()
    .autoProvideVariables({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
    })

    // enables @babel/preset-env polyfills
    /*.configureBabel(() => {}, {
        useBuiltIns: 'usage',
        corejs: 3
    })*/

    // COPY FILES
    .copyFiles({
        from: './templates/assets/img',
        to: 'img/[name].[ext]',
        //pattern: /\.(png|jpg|jpeg)$/
    })
    .copyFiles({
        from: './templates/assets/fonts',
        to: 'fonts/[name].[ext]'
        //pattern: /\.(png|jpg|jpeg)$/
    })
    .copyFiles({
        from: './templates/assets/favicons',
        to: 'favicons/[name].[ext]'
        //pattern: /\.(png|jpg|jpeg)$/
    })

    .splitEntryChunks()
    .enableSingleRuntimeChunk()
;

var config = Encore.getWebpackConfig();

if (!Encore.isProduction()) {
    config.devServer.watchOptions = {
        watch: true,
        poll: true,
        ignored: /node_modules/
    };
}
config.resolve.extensions.push('json');

module.exports = config;
