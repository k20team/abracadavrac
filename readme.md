Abracadavrac est un localisateur de vrac.

Auteur : SARL K2.0

Stack technique
 - Base de données  : Postgresql 
    + Extension postgis pour les calculs géographiques
    + Extension Unaccent pour les requêtes SQL sans tenir compte des accents
 - Framework PHP Symfony4
 - Front : webpack + vuejs

Règle d'utilisation du GIT : 
 - master = prod
 - test = test
Chanque fonctionnalité doit être développée sur une branche. Les branches sont nommées de la façon suivante : 
numero_ticket_trello-nom_développeur-commentaire_fonctionnalité

Exemple : 101_npetit_signalement_produit

Installation 
   - composer install
   - npm install
   - php bin/console doctrine:migration:migrate
   - php bin/console doctrine:fixtures:load
   - php bin/console task:command refresh_product_images >> var/log/product_images.log