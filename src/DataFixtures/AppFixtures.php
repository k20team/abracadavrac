<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
use App\Entity\Shop;
use App\Entity\Address;
use App\Entity\Label;
use App\Entity\MeasureUnit;
use App\Entity\Product;
use App\Entity\ProductImage;
use App\Entity\ProductType;
use App\Entity\ProductUsage;
use App\Entity\ShopType;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\HttpFoundation\File\File;

class AppFixtures extends Fixture
{
    private $encoder;
    private $entityManager;
    private $publicDir;

    public function __construct(
        UserPasswordEncoderInterface $encoder,
        ObjectManager $entityManager,
        $publicDir)
    {
        $this->encoder = $encoder;
        $this->entityManager = $entityManager;
        $this->publicDir = $publicDir;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadUsers();
        $manager->flush();

        $this->loadSettings();
        $manager->flush();

        $this->loadShops();
        $manager->flush();

        $this->loadRealShops();
        $manager->flush();

    }

    private function loadUsers(): Void {
        $admin = new User();
        $admin->setEmail('admin@abracada-vrac.com');
        $admin->setFirstname('admin');
        $admin->setLastname('admin');
        $admin->setRoles(["ROLE_ADMIN"]);
        $admin->setIsEnabled(true);
        $admin->setPassword($this->encoder->encodePassword($admin, 'admin'));
        $toPersist[] = $admin;

        foreach($toPersist as $entity) {
            $this->entityManager->persist($entity);
        }

        $this->admin = $admin;
    }

    private function loadSettings() {
        /*- Bio
        - Territoire protégé
        - Commerce équitable
        - Végan
        - Sans gluten
        - Bleu blanc coeur
        - Slow cosmetique*/
        $toPersist[] = (new Label())
            ->setName('Bio')
            ->setDescription('')
            ->setIcon('bio');
        $toPersist[] = (new Label())
            ->setName('Territoire protégé')
            ->setDescription('')
            ->setIcon('territoire-protege');
        $toPersist[] = (new Label())
            ->setName('Commerce équitable')
            ->setDescription('')
            ->setIcon('commerce-equitable');
        $toPersist[] = (new Label())
            ->setName('Végan')
            ->setDescription('')
            ->setIcon('vegan');
        $toPersist[] = (new Label())
            ->setName('Sans gluten')
            ->setDescription('')
            ->setIcon('sans-gluten');
        $toPersist[] = (new Label())
            ->setName('Bleu blanc coeur')
            ->setDescription('')
            ->setIcon('bleu-blanc-coeur');
        $toPersist[] = (new Label())
            ->setName('Slow cosmetique')
            ->setDescription('')
            ->setIcon('slow-cosmetique');

        $toPersist[] = (new MeasureUnit())
                        ->setName('milligrame')
                        ->setShortName('mg');
        $toPersist[] = (new MeasureUnit())
                        ->setName('gramme')
                        ->setShortName('g');
        $toPersist[] = (new MeasureUnit())
                        ->setName('100g')
                        ->setShortName('100g');
        $toPersist[] = (new MeasureUnit())
                        ->setName('kilo')
                        ->setShortName('kg');
        $toPersist[] = (new MeasureUnit())
                        ->setName('millilitre')
                        ->setShortName('ml');
        $toPersist[] = (new MeasureUnit())
                        ->setName('centilitre')
                        ->setShortName('cl');
        $toPersist[] = (new MeasureUnit())
                        ->setName('litre')
                        ->setShortName('L');
        $toPersist[] = (new MeasureUnit())
                        ->setName('pièce')
                        ->setShortName('pièce');
        
        
        
        $toPersist[] = (new ProductType())->setName('Aromates, Condiments, Épices')->setAppearanceOrder(1)->setIcon('aromates-condiments-epices')->setDescription("N'hésitez pas à récupérer vos épices en vrac : poivres, piments, cannelles, curry sont disponibles en vrac. L'avantage du vrac pour ce type de produit : récuperez ce dont vous avez besoin et faites des économies !");
        $toPersist[] = (new ProductType())->setName('Bases de cuisine')->setAppearanceOrder(2)->setIcon('bases-de-cuisine')->setDescription("Huiles, Farines, Sucres, Sels etc.. Des produits que nous utilisons tout les jours pour cuisiner. 2 avantages à acheter ces produits en vrac : on peut en acheter la quantité dont on estime avoir besoin pour faire des économies et on réduit ainsi ses déchets perso avec efficacité");
        $toPersist[] = (new ProductType())->setName('Boissons')->setAppearanceOrder(3)->setIcon('boissons')->setDescription("Pour vos boissons, n'hésitez pas à tester des contenants de grande contenance. N'oubliez pas qu'il est possible de trouver du vin en vrac ou des bières consignée / en bouteilles consignées");
        $toPersist[] = (new ProductType())->setName('Cafés, Thés, Plantes')->setAppearanceOrder(4)->setIcon('cafes-thes-plantes')->setDescription("Vous êtes de grands fan de cafés ou de thés, alors n'hésitez pas à les acheter en vrac !");
        $toPersist[] = (new ProductType())->setName('Entretien, Animaux & Accessoires')->setAppearanceOrder(5)->setIcon('entretien-animaux-accessoires')->setDescription("Les magasins vrac proposent souvent deux options pour vos produits d’entretien : les ingrédients pour les faire maison ou les produits finis, à prendre au robinet dans de gros bidons de produits d’entretien en vrac.");
        $toPersist[] = (new ProductType())->setName('Épicerie salée')->setAppearanceOrder(6)->setIcon('epicerie-salee')->setDescription("Pour remplacer les chips, rien de tel que les biscuits salés en vrac. Retrouver dans ce rayon les produits pour l'apéro : tapenades, olive,cacahuètes, etc... tout ça en vrac !");
        $toPersist[] = (new ProductType())->setName('Épicerie sucrée')->setAppearanceOrder(7)->setIcon('epicerie-sucree')->setDescription("Dans l'épicerie sucrée, retrouvez des confitures en vrac, des céréales pour le petit déjeuner, des confiseries et bonbons, du miel, des pates à tartiner et bien d'autres produits en vrac.");
        $toPersist[] = (new ProductType())->setName('Fruits secs, Graines')->setAppearanceOrder(8)->setIcon('fruits-secs-graines')->setDescription("Granies de lin, graines de courges, graines de chia, mélange de céréales, abricots secs, tout ses produits disponible en vrac dans les environs");
        $toPersist[] = (new ProductType())->setName('Hygiène & Santé')->setAppearanceOrder(9)->setIcon('hygiene-sante')->setDescription("Le vrac est l'occasion de revoir sa consommation et son utilisation de produits hygiénique industriels : shampoings et savons solide pour réduire les déchets; des compositions simples et transparentes pour éviter les dangers sanitaires");
        $toPersist[] = (new ProductType())->setName('Légumineuses & Céréales')->setAppearanceOrder(10)->setIcon('legumineuse-cereales')->setDescription("Les céréales et tout les autres féculents sont secs et donc faciles à transporter avec un bocal ou un sac à vrac");
        $toPersist[] = (new ProductType())->setName('Pâtes & Riz')->setAppearanceOrder(11)->setIcon('pates-riz')->setDescription("Pâtes, riz et légumineuses sont les premiers produits à être vendu en vrac. N'hésiter pas à utiliser vos bocaux et/ou sacs à vrac pour venir récupérer");
        $toPersist[] = (new ProductType())->setName('Produits frais')->setAppearanceOrder(12)->setIcon('produits-frais')->setDescription("Les épiceries vrac offrent l'avantage de vendre de bons produits frais, donc locaux et artisanaux, souvent bios et d'une excellente qualité. N'oubliez pas les contenants, tupperwares et bocaux pour récupérez vos fromages");

        $toPersist[] = (new ProductUsage())->setName('Apéro')->setAppearanceOrder(1)->setCoverName('cover-1-5e709ac53088b.jpg');
        $toPersist[] = (new ProductUsage())->setName('DIY')->setAppearanceOrder(2)->setCoverName('cover-2-5e709abac8cca.jpg');
        $toPersist[] = (new ProductUsage())->setName('Menager')->setAppearanceOrder(3)->setCoverName('');
        $toPersist[] = (new ProductUsage())->setName('Enfant / Bébé ')->setAppearanceOrder(4)->setCoverName('cover-4-5e709a9d3033e.jpg');
        $toPersist[] = (new ProductUsage())->setName('Petit déjeuner')->setAppearanceOrder(5)->setCoverName('cover-5-5e709aaa4a721.jpg');
        $toPersist[] = (new ProductUsage())->setName('Gouter')->setAppearanceOrder(6)->setCoverName('');

        foreach($toPersist as $entity) {
            $this->entityManager->persist($entity);
        }

        $epicerie = (new ShopType())->setName('Épicerie');
        $epicerie2 = (new ShopType())->setName('Épicerie 100% vrac');
        $this->entityManager->persist($epicerie);
        $this->entityManager->persist($epicerie);
        $this->entityManager->persist($epicerie2);
        $this->entityManager->persist($epicerie2);
    }

    private function loadShops() {
        $epicerieType = $this->entityManager->getRepository(ShopType::class)->findOneByName('Épicerie');
        $shop = new Shop();
        $shop->setName('L\'épicerie equitable');
        $shop->addType($epicerieType);
        $address = new Address();
        $address->setAddress('78 Rue Montesquieu 69007 Lyon');
        $address->setLat(45.751403);
        $address->setLng(4.844729);
        $address->updateLocation();
        $shop->setAddress($address);
        $this->entityManager->persist($shop);
        $this->entityManager->persist($address);

        $shop = new Shop();
        $shop->setName('3 p\'tits poids');
        $shop->addType($epicerieType);
        $address = new Address();
        $address->setAddress('124 Rue Sébastien Gryphe 69007 Lyon');
        $address->setLat(45.748327);
        $address->setLng(4.839318);
        $address->updateLocation();
        $shop->setAddress($address);
        $this->entityManager->persist($shop);
        $this->entityManager->persist($address);

        $shop = new Shop();
        $shop->setName('La Vie Claire');
        $shop->addType($epicerieType);
        $address = new Address();
        $address->setAddress('4 Boulevard de la Turdine, 69170 Tarare');
        $address->setLat(45.888621);
        $address->setLng(4.455347);
        $address->updateLocation();
        $shop->setAddress($address);
        $this->entityManager->persist($shop);
        $this->entityManager->persist($address);
    }

    private function loadProducts() {
        
        //--Création produits
        $productTypes = $this->entityManager->getRepository(ProductType::class)->findAll();
        $productUsages = $this->entityManager->getRepository(ProductUsage::class)->findAll();
        $units = $this->entityManager->getRepository(MeasureUnit::class)->findAll();
        $shops = $this->entityManager->getRepository(Shop::class)->findAll();
        $origins = [Product::ORIGIN_REGIONAL, Product::ORIGIN_FRANCE, Product::ORIGIN_EUROPE, Product::ORIGIN_WORLD];
        $names = ['TEST - Noix de cajou', 'TEST - Farine B55', 'TEST - Fraises', 'TEST - Savon', 'TEST - Biscuits', 'TEST - Brosse à dents', 'TEST - Raisins secs', 'TEST - Mélange de céréales'];

        //--Création de 100 produits randoms
        for($i=0; $i<100; $i++) {
            $shop = $shops[rand(0, (count($shops) -1))];
            $product = new Product();
            $product->setName($names[rand(0, (count($names) -1))]);
            $product->setType($productTypes[rand(0, (count($productTypes) -1))]);
            $product->addUsage($productUsages[rand(0, (count($productUsages) -1))]);
            $product->setOrigin($origins[rand(0, (count($origins) -1))]);
            $product->setPrice(rand(0,20));
            $product->setUnit($units[rand(0, (count($units) -1))]);
            $product->setShop($shop);

            $this->entityManager->persist($product);
        }
    }

    private function loadRealShops() {
        $epicerieType = $this->entityManager->getRepository(ShopType::class)->findOneByName('Épicerie');
        $shop = new Shop();
        $shop->setName('Eldora d\'Oz');
        $shop->addType($epicerieType);
        $address = new Address();
        $address->setAddress('7 Rue Laporte, 69009 Lyon');
        $address->setLat(45.778564);
        $address->setLng(4.804763);
        $address->updateLocation();
        $shop->setAddress($address);
        $this->entityManager->persist($shop);
        $this->entityManager->persist($address);
        $this->entityManager->flush();

        //$appPath = $this->container->getParameter('kernel.root_dir');
        $file = realpath($this->publicDir . '/fixtures_imports/EldoradOz.csv');
        $encoder = new CsvEncoder();
        $datas = $encoder->decode(file_get_contents($file), 'csv', [CsvEncoder::DELIMITER_KEY => ";"]);
        array_shift($datas);
        $this->importProductCsv($datas, $shop);


        $shop = new Shop();
        $shop->setName('A la source');
        $shop->addType($epicerieType);
        $address = new Address();
        $address->setAddress('63 Rue de la Part-Dieu, 69003 Lyon');
        $address->setLat(45.759638);
        $address->setLng(4.847186);
        $address->updateLocation();
        $shop->setAddress($address);
        $this->entityManager->persist($shop);
        $this->entityManager->persist($address);
        $this->entityManager->flush();

        //$appPath = $this->container->getParameter('kernel.root_dir');
        $file = realpath($this->publicDir . '/fixtures_imports/Alasource.csv');
        $encoder = new CsvEncoder();
        $datas = $encoder->decode(file_get_contents($file), 'csv', [CsvEncoder::DELIMITER_KEY => ";"]);
        array_shift($datas);
        $this->importProductCsv($datas, $shop);
    }

    private function importProductCsv($datas, $shop) {
        foreach($datas as $data) {
            try {
                $dataIndex = array_values($data);
                $product = new Product();
                $product->setName($dataIndex[0]);
                $product->setDescription($dataIndex[1]);
                $product->setPrice($dataIndex[2]);
                $unit = $this->entityManager->getRepository(MeasureUnit::class)->findOneByShortName(trim($dataIndex[3]));
                $product->setUnit($unit);
                switch (trim($dataIndex[4])) {
                    case 'Monde':
                        $product->setOrigin(Product::ORIGIN_WORLD);
                        break;
                    case 'UE':
                        $product->setOrigin(Product::ORIGIN_EUROPE);
                        break;
                    case 'Europe':
                        $product->setOrigin(Product::ORIGIN_EUROPE);
                        break;
                    case 'France':
                        $product->setOrigin(Product::ORIGIN_FRANCE);
                        break;
                    case 'Région':
                        $product->setOrigin(Product::ORIGIN_REGIONAL);
                        break;
                    default:
                        break;
                }
                $labels = explode(",", $dataIndex[5]);
                foreach($labels as $label_string) {
                    $label = $this->entityManager->getRepository(Label::class)->findOneByName(trim($label_string));
                    if ($label !== null) {
                        $product->addLabel($label);
                    }
                }
    
                $categorie = $this->entityManager->getRepository(ProductType::class)->findOneByName(trim($dataIndex[6]));
                if ($categorie !== null) {
                    $product->setType($categorie);
                }
    
                $usages = explode(",", $dataIndex[7]);
                foreach($usages as $usage_string) {
                    $usage = $this->entityManager->getRepository(ProductUsage::class)->findOneByName(trim($usage_string));
                    if ($usage !== null) {
                        $product->addUsage($usage);
                    }
                }
    
                $product->setProducer($dataIndex[8]);
                $product->setShop($shop);
    
                $this->entityManager->persist($product);
                $this->entityManager->flush();
            } catch(Exception $e) {
                dump($data);
                dump($e);
            }
            
        }
    }

    /*private function lodRealProductImages() {
        //--On vidange les répertoires avant le script
        $path = $this->publicDir.$this->productImagePath;
        $derep=opendir($path);
        while($file = readdir($derep)){
            if($file != '..' && $file !='.' && $file !='' && $file!='.htaccess'){
                unlink($path.$file);
            }
        }
        $path = $this->publicDir."media/cache/product_thumb/images/products/";
        $derep=opendir($path);
        while($file = readdir($derep)){
            if($file != '..' && $file !='.' && $file !='' && $file!='.htaccess'){
                unlink($path.$file);
            }
        }

        //--On va récupérer les catégories
        $categories = $this->entityManager->getRepository(ProductType::class)->findAll();

        //--On les parcours
        foreach($categories as $cat) {
            //--On va chercher le dossier correspondant à la catégorie
            $dir = realpath($this->publicDir . '/fixtures_imports/images_produits/'.$cat->getName());
            if ($dir !== false) {
                $files = scandir($dir);
                //--On va parcourir les fichiers du dossier
                foreach ($files as $file) {
                    if ($file !== '.' && $file !== '..') {
                        //--On créé l'objet
                        $productImage = new ProductImage();
                        $name = explode('.', $file)[0];
                        $productImage->setName($name);
                        $productImage->setProductType($cat);
                        if (!copy($dir.'/'.$file, $this->publicDir.$this->productImagePath.$file)) {
                            echo "La copie $file du fichier a échoué...\n";
                        } else {
                            $symfonyFile = new File($this->publicDir.$this->productImagePath.$file);
                            $productImage->setImageFile($symfonyFile);
                            $finalName = $productImage->getFileNamer();
                            rename($this->publicDir.$this->productImagePath.$file, $this->publicDir.$this->productImagePath.$finalName.".".$symfonyFile->getExtension());
                            $productImage->setImageName($finalName.".".$symfonyFile->getExtension());
                            $this->entityManager->persist($productImage);
                            $this->entityManager->flush();
                            $image = $this->liipImagine->getUrlOfFilteredImage($this->productImagePath.$productImage->getImageName(), 'product_thumb');
                        }
                    }
                }
            }
        }
    }*/
}
