<?php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use App\Entity\OpeningTimes;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('getDayFR', [$this, 'dayFr']),
        ];
    }

    public function dayFr($day)
    {
        if ($day == OpeningTimes::DAY_MONDAY) $day = 'lundi';
        if ($day == OpeningTimes::DAY_TUESDAY) $day = 'mardi';
        if ($day == OpeningTimes::DAY_WEDNESDAY) $day = 'mercredi';
        if ($day == OpeningTimes::DAY_THURSDAY) $day = 'jeudi';
        if ($day == OpeningTimes::DAY_FRIDAY) $day = 'vendredi';
        if ($day == OpeningTimes::DAY_SATURDAY) $day = 'samedi';
        if ($day == OpeningTimes::DAY_SUNDAY) $day = 'dimanche';

        return $day;
    }
}
