<?php
namespace App\EventListener;

use App\Entity\ProductType;
use App\Entity\ProductUsage;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Environment;
use App\Entity\User;
use App\Form\SearchForm;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ControllerListener implements EventSubscriberInterface
{
    private $authorizationChecker;
    private $tokenStorage;
    private $currentUserService;
    private $router;
    private $twig;
    private $em;
    private $formFactory;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        TokenStorageInterface $tokenStorage,
        UrlGeneratorInterface $router,
        Environment $twig,
        EntityManagerInterface $em,
        FormFactoryInterface $formFactory
    ) {
        $this->authorizationChecker = $authorizationChecker;
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
        $this->twig = $twig;
        $this->em = $em;
        $this->formFactory = $formFactory;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();
        $currentRoute = $event->getRequest()->get('_route');
        $currentUrl = $event->getRequest()->getUri();
        $url = null;

        /*if ($currentRoute !== 'search_product') {
            $searchForm = $this->formFactory->create(SearchForm::class, null, array(
                'action' => $this->router->generate('search_product'),
                'method' => 'GET'
            ))->createView();
            $this->twig->addGlobal('searchForm', $searchForm);
        }*/

        $this->twig->addGlobal('globalProductsTypes', $this->em->getRepository(ProductType::class)->findAll());
        $this->twig->addGlobal('globalProductsUsages', $this->em->getRepository(ProductUsage::class)->findAll());

        if($this->tokenStorage->getToken() != null){
            $user = $this->tokenStorage->getToken()->getUser();
            if ($user instanceof User && $this->authorizationChecker->isGranted('ROLE_ADMIN', $user) && preg_match('/admin/', $currentUrl) ) {
                return;
            }
        }

        if (is_array($controller) && $controller[0] instanceof AbstractController) {

            if ($user instanceof User) {

                $managedShops = $user->getManagedShops();
                $this->twig->addGlobal('managedShops', $managedShops);

                //--On redirige les utilisateurs connecté qui éssaient de s'inscrire ou de se logger
                if (in_array($currentRoute, ['login'])) {
                    $url = $this->router->generate('homepage');
                }
            }

            if($url != null){
                $event->setController(function() use ($url) {
                    return new RedirectResponse($url);
                });
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }
}
