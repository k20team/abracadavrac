<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="user_user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=65)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=65)
     */
    private $lastname;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $resetPassword;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isTest;

    /**
     * @ORM\OneToMany(targetEntity=ProductSignal::class, mappedBy="correctedBy")
     */
    private $productSignals;

    /**
     * @ORM\ManyToMany(targetEntity=Shop::class, mappedBy="managedBy")
     */
    private $managedShops;

    public function __toString()
    {
        return $this->getFirstname().' '.$this->getLastname().' ('.$this->getEmail().')';
    }

    /**
     * {@inheritdoc}
     */
    public function serialize(): string
    {
        return serialize([$this->id, $this->email, $this->password]);
    }
    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized): void
    {
        [$this->id, $this->email, $this->password] = unserialize($serialized, ['allowed_classes' => false]);
    }

    public function hasRole($role) {
        return in_array($role, $this->getRoles());
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    public function __construct()
    {
        $this->isEnabled = true;
        $this->isConfirmed = false;
        $this->isTest = false;
        $this->productSignals = new ArrayCollection();
        $this->managedShops = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getIsEnabled(): ?bool
    {
        return $this->isEnabled;
    }

    public function setIsEnabled(bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    public function getIsTest(): ?bool
    {
        return $this->isTest;
    }

    public function setIsTest(bool $isTest): self
    {
        $this->isTest = $isTest;

        return $this;
    }

    public function getResetPassword(): ?string
    {
        return $this->resetPassword;
    }

    public function setResetPassword(?string $resetPassword): self
    {
        $this->resetPassword = $resetPassword;

        return $this;
    }

    /**
     * @return Collection|ProductSignal[]
     */
    public function getProductSignals(): Collection
    {
        return $this->productSignals;
    }

    public function addProductSignal(ProductSignal $productSignal): self
    {
        if (!$this->productSignals->contains($productSignal)) {
            $this->productSignals[] = $productSignal;
            $productSignal->setCorrectedBy($this);
        }

        return $this;
    }

    public function removeProductSignal(ProductSignal $productSignal): self
    {
        if ($this->productSignals->contains($productSignal)) {
            $this->productSignals->removeElement($productSignal);
            // set the owning side to null (unless already changed)
            if ($productSignal->getCorrectedBy() === $this) {
                $productSignal->setCorrectedBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Shop[]
     */
    public function getManagedShops(): Collection
    {
        return $this->managedShops;
    }

    public function addManagedShop(Shop $managedShop): self
    {
        if (!$this->managedShops->contains($managedShop)) {
            $this->managedShops[] = $managedShop;
            $managedShop->addManagedBy($this);
        }

        return $this;
    }

    public function removeManagedShop(Shop $managedShop): self
    {
        if ($this->managedShops->contains($managedShop)) {
            $this->managedShops->removeElement($managedShop);
            $managedShop->removeManagedBy($this);
        }

        return $this;
    }
}
