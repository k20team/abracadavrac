<?php

namespace App\Entity;

use App\Repository\ProductSignalRepository;
use JMS\Serializer\Annotation as JMS;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductSignalRepository::class)
 */
class ProductSignal
{
    const TYPE_STOCK = 'stock';
    const TYPE_PRICE = 'price';
    const STATUS_OPEN = 'open';
    const STATUS_CLOSED = 'closed';


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=31)
     * @JMS\Expose
     * @JMS\Groups({"product_search"})
     */
    private $type;

    /**
     * @ORM\Column(type="datetime")
     * @JMS\Expose
     * @JMS\Groups({"product_search"})
     */
    private $signaledAt;

    /**
     * @ORM\Column(type="string", length=31)
     * @JMS\Expose
     * @JMS\Groups({"product_search"})
     */
    private $status;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $correctedAt;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="signals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="productSignals")
     */
    private $correctedBy;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    public function __construct()
    {
        $this->setStatus(self::STATUS_OPEN);
        $this->setSignaledAt(new \DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSignaledAt(): ?\DateTimeInterface
    {
        return $this->signaledAt;
    }

    public function setSignaledAt(\DateTimeInterface $signaledAt): self
    {
        $this->signaledAt = $signaledAt;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }


    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getCorrectedBy(): ?User
    {
        return $this->correctedBy;
    }

    public function setCorrectedBy(?User $correctedBy): self
    {
        $this->correctedBy = $correctedBy;

        return $this;
    }

    public function getCorrectedAt(): ?\DateTimeInterface
    {
        return $this->correctedAt;
    }

    public function setCorrectedAt(?\DateTimeInterface $correctedAt): self
    {
        $this->correctedAt = $correctedAt;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }
}
