<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @Vich\Uploadable
 * @JMS\ExclusionPolicy("all")
 */
class Product
{
    const ORIGIN_REGIONAL = '1_regional';
    const ORIGIN_FRANCE = '2_france';
    const ORIGIN_EUROPE = '3_europe';
    const ORIGIN_WORLD = '4_world';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Expose
     * @JMS\Groups({"product_search"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Expose
     * @JMS\Groups({"product_search"})
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"product_search"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=64)
     * @JMS\Expose
     * @JMS\Groups({"product_search"})
     */
    private $origin;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"product_search"})
     */
    private $producer;

    /**
     * @ORM\Column(type="float")
     * @JMS\Expose
     * @JMS\Groups({"product_search"})
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MeasureUnit", inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Expose
     * @JMS\Groups({"product_search"})
     */
    private $unit;


    /**
     * @ORM\Column(type="datetime")
     * @JMS\Expose
     * @JMS\Groups({"product_search"})
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop", inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Expose
     * @JMS\Groups({"product_search"})
     */
    private $shop;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Label", inversedBy="products")
     * @JMS\Expose
     * @JMS\Groups({"product_search"})
     */
    private $labels;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ProductUsage", inversedBy="products")
     * @JMS\Expose
     * @JMS\Groups({"product_search"})
     */
    private $usages;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProductType", inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Expose
     * @JMS\Groups({"product_search"})
     */
    private $type;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPublished;

    /**
     * @ORM\ManyToOne(targetEntity=ProductImage::class, inversedBy="products")
     * @JMS\Expose
     * @JMS\Groups({"product_search"})
     */
    private $productImage;

    private $distance;

    /**
     * @ORM\OneToMany(targetEntity=ProductSignal::class, mappedBy="product", orphanRemoval=true, cascade={"persist", "remove"})
     * @JMS\Expose
     * @JMS\Groups({"product_search"})
     */
    private $signals;

    public function getSignalsPriceNbr() {
        $nbr = 0;
        foreach ($this->getSignals() as $signal) {
            if ($signal->getType() == ProductSignal::TYPE_PRICE && $signal->getStatus() == ProductSignal::STATUS_OPEN) {
                $nbr++;
            }
        }
        return $nbr;
    }

    public function getSignalsStockNbr() {
        $nbr = 0;
        foreach ($this->getSignals() as $signal) {
            if ($signal->getType() == ProductSignal::TYPE_STOCK && $signal->getStatus() == ProductSignal::STATUS_OPEN) {
                $nbr++;
            }
        }
        return $nbr;
    }

    public function setDistance($distance) {
        $this->distance = $distance;
    }
    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("distance")
     * @JMS\Expose
     * @JMS\Groups({"product_search"})
     */
    public function getDistance()
    {
        return $this->distance;
    }

    public function priceToString() {
        return $this->price."€ / ".$this->unit->getShortName()." (".$this->updatedAt->format('d/m/Y').")";
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function __construct()
    {
        $this->updatedAt = new \DateTime();
        $this->labels = new ArrayCollection();
        $this->usages = new ArrayCollection();
        $this->isPublished = true;

        $this->signals = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getOrigin(): ?string
    {
        return $this->origin;
    }

    public function setOrigin(string $origin): self
    {
        $this->origin = $origin;

        return $this;
    }

    public function getProducer(): ?string
    {
        return $this->producer;
    }

    public function setProducer(?string $producer): self
    {
        $this->producer = $producer;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getUnit(): ?MeasureUnit
    {
        return $this->unit;
    }

    public function setUnit(?MeasureUnit $unit): self
    {
        $this->unit = $unit;

        return $this;
    }

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(?Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * @return Collection|Label[]
     */
    public function getLabels(): Collection
    {
        return $this->labels;
    }

    public function addLabel(Label $label): self
    {
        if (!$this->labels->contains($label)) {
            $this->labels[] = $label;
        }

        return $this;
    }

    public function removeLabel(Label $label): self
    {
        if ($this->labels->contains($label)) {
            $this->labels->removeElement($label);
        }

        return $this;
    }

    /**
     * @return Collection|usage[]
     */
    public function getUsages(): Collection
    {
        return $this->usages;
    }

    public function addUsage(ProductUsage $usages): self
    {
        if (!$this->usages->contains($usages)) {
            $this->usages[] = $usages;
        }

        return $this;
    }

    public function removeUsage(ProductUsage $usages): self
    {
        if ($this->usages->contains($usages)) {
            $this->usages->removeElement($usages);
        }

        return $this;
    }

    public function getType(): ?ProductType
    {
        return $this->type;
    }

    public function setType(?ProductType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    public function getProductImage(): ?ProductImage
    {
        return $this->productImage;
    }

    public function setProductImage(?ProductImage $productImage): self
    {
        $this->productImage = $productImage;

        return $this;
    }

    /**
     * @return Collection|ProductSignal[]
     */
    public function getSignals(): Collection
    {
        return $this->signals;
    }

    public function addSignal(ProductSignal $signal): self
    {
        if (!$this->signals->contains($signal)) {
            $this->signals[] = $signal;
            $signal->setProduct($this);
        }

        return $this;
    }

    public function removeSignal(ProductSignal $signal): self
    {
        if ($this->signals->contains($signal)) {
            $this->signals->removeElement($signal);
            // set the owning side to null (unless already changed)
            if ($signal->getProduct() === $this) {
                $signal->setProduct(null);
            }
        }

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

}
