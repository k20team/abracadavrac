<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AddressRepository")
 * @JMS\ExclusionPolicy("all")
 */
class Address
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Expose
     * @JMS\Groups({"product_search", "shop_search"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Expose
     * @JMS\Groups({"product_search", "shop_search"})
     */
    private $address;

    /**
     * @ORM\Column(type="float", precision=10, scale=2)
     * @JMS\Expose
     * @JMS\Groups({"product_search", "shop_search"})
     */
    private $lng;

    /**
     * @ORM\Column(type="float", precision=10, scale=2)
     * @JMS\Expose
     * @JMS\Groups({"product_search", "shop_search"})
     */
    private $lat;

    /**
     * @ORM\Column(type="geography", options={"geometry_type"="POINT"})
     */
    private $location;


    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Shop", mappedBy="address", cascade={"persist", "remove"})
     */
    private $shop;

    //private $point = null;

    public function updateLocation()
    {
        if (isset($this->lat) && isset($this->lng)) {
            $this->location = sprintf(
             'POINT(%f %f)',
             (string) $this->lng,
             (string) $this->lat
           );

           $this->point = null;
       }
    }

    public function __toString()
    {
        return $this->getAddress();
    }

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function setLocation($location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(Shop $shop): self
    {
        $this->shop = $shop;

        // set the owning side of the relation if necessary
        if ($this !== $shop->getAddress()) {
            $shop->setAddress($this);
        }
        return $this;
    }

    public function getLng(): ?float
    {
        return $this->lng;
    }

    public function setLng(float $lng): self
    {
        $this->lng = $lng;

        return $this;
    }

    public function getLat(): ?float
    {
        return $this->lat;
    }

    public function setLat(float $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }
}
