<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShopRepository")
 * @Vich\Uploadable
 * @JMS\ExclusionPolicy("all")
 */
class Shop
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Expose
     * @JMS\Groups({"product_search", "shop_search"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Expose
     * @JMS\Groups({"product_search", "shop_search"})
     */
    private $name;
    
    /**
    * @Gedmo\Slug(fields={"name"})
    * @ORM\Column(length=128, unique=true)
     * @JMS\Expose
     * @JMS\Groups({"product_search", "shop_search"})
    */
   private $slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Address", inversedBy="shop", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Expose
     * @JMS\Groups({"product_search", "shop_search"})
     */
    private $address;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="shop", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $products;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ShopType", inversedBy="shops")
     * @ORM\JoinTable(name="shop_type_shop")
     * @JMS\Expose
     * @JMS\Groups({"shop_search"})
     */
    private $types;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPublished;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OpeningTimes", mappedBy="shop", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"dayNumber" = "ASC"})
     */
    private $openingTimes;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logoName;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $uploadedAt;

    private $distance;
    public function setDistance($distance) {
        $this->distance = $distance;
    }
    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("distance")
     * @JMS\Expose
     * @JMS\Groups({"product_search", "shop_search"})
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("today_opening_times")
     * @JMS\Expose
     * @JMS\Groups({"product_search", "shop_search"})
     */
    public function getTodayOpeningTimes()
    {
        $day_of_week = date('N', (new \DateTime())->getTimestamp()) -1;
        return $this->getOpeningTimes()[$day_of_week];
    }

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="shop_logo", fileNameProperty="logoName")
     *
     * @var File
     */
    private $logoFile;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="managedShops")
     */
    private $managedBy;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $logoFile
     */
    public function setLogoFile(?File $logoFile = null): void
    {
        $this->logoFile = $logoFile;
        if (null !== $logoFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->uploadedAt = new \DateTimeImmutable();
        }
    }

    public function getLogoFile(): ?File
    {
        return $this->logoFile;
    }
    /**
     * Get file namer
     *
     * @return string
     */
    public function getFileNamer()
    {
        return sprintf('logo-%d-%s', $this->getId(), uniqid());
    }

    public function __toString()
    {
        return (string) $this->name;
    }

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->types = new ArrayCollection();
        $this->isPublished = true;
        $this->openingTimes = new ArrayCollection();
        $this->addOpeningTime((new OpeningTimes())->setDay(OpeningTimes::DAY_MONDAY)->setDayNumber(1));
        $this->addOpeningTime((new OpeningTimes())->setDay(OpeningTimes::DAY_TUESDAY)->setDayNumber(2));
        $this->addOpeningTime((new OpeningTimes())->setDay(OpeningTimes::DAY_WEDNESDAY)->setDayNumber(3));
        $this->addOpeningTime((new OpeningTimes())->setDay(OpeningTimes::DAY_THURSDAY)->setDayNumber(4));
        $this->addOpeningTime((new OpeningTimes())->setDay(OpeningTimes::DAY_FRIDAY)->setDayNumber(5));
        $this->addOpeningTime((new OpeningTimes())->setDay(OpeningTimes::DAY_SATURDAY)->setDayNumber(6));
        $this->addOpeningTime((new OpeningTimes())->setDay(OpeningTimes::DAY_SUNDAY)->setDayNumber(7));
        $this->managedBy = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(Address $address): self
    {
        $this->address = $address;

        // set the owning side of the relation if necessary
        if ($this !== $address->getShop()) {
            $address->setShop($this);
        }

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setShop($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getShop() === $this) {
                $product->setShop(null);
            }
        }

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|ShopType[]
     */
    public function getTypes(): Collection
    {
        return $this->types;
    }

    public function addType(ShopType $type): self
    {
        if (!$this->types->contains($type)) {
            $this->types[] = $type;
        }

        return $this;
    }

    public function removeType(ShopType $type): self
    {
        if ($this->types->contains($type)) {
            $this->types->removeElement($type);
        }

        return $this;
    }

    public function getLogoName(): ?string
    {
        return $this->logoName;
    }

    public function setLogoName(?string $logoName): self
    {
        $this->logoName = $logoName;

        return $this;
    }

    public function getUploadedAt(): ?\DateTimeInterface
    {
        return $this->uploadedAt;
    }

    public function setUploadedAt(?\DateTimeInterface $uploadedAt): self
    {
        $this->uploadedAt = $uploadedAt;

        return $this;
    }

    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * @return Collection|OpeningTimes[]
     */
    public function getOpeningTimes(): Collection
    {
        return $this->openingTimes;
    }

    public function addOpeningTime(OpeningTimes $openingTime): self
    {
        if (!$this->openingTimes->contains($openingTime)) {
            $this->openingTimes[] = $openingTime;
            $openingTime->setShop($this);
        }

        return $this;
    }

    public function removeOpeningTime(OpeningTimes $openingTime): self
    {
        if ($this->openingTimes->contains($openingTime)) {
            $this->openingTimes->removeElement($openingTime);
            // set the owning side to null (unless already changed)
            if ($openingTime->getShop() === $this) {
                $openingTime->setShop(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getManagedBy(): Collection
    {
        return $this->managedBy;
    }

    public function addManagedBy(User $managedBy): self
    {
        if (!$this->managedBy->contains($managedBy)) {
            $this->managedBy[] = $managedBy;
        }

        return $this;
    }

    public function removeManagedBy(User $managedBy): self
    {
        if ($this->managedBy->contains($managedBy)) {
            $this->managedBy->removeElement($managedBy);
        }

        return $this;
    }
}
