<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OpeningTimesRepository")
 * @JMS\ExclusionPolicy("all")
 */
class OpeningTimes
{
    const DAY_MONDAY = 'monday';
    const DAY_TUESDAY = 'tuesday';
    const DAY_WEDNESDAY = 'wednesday';
    const DAY_THURSDAY = 'thursday';
    const DAY_FRIDAY = 'friday';
    const DAY_SATURDAY = 'saturday';
    const DAY_SUNDAY = 'sunday';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Expose
     * @JMS\Groups({"product_search", "shop_search"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=15)
     * @JMS\Expose
     * @JMS\Groups({"product_search", "shop_search"})
     */
    private $day;

    /**
     * @ORM\Column(type="integer")
     * @JMS\Expose
     * @JMS\Groups({"shop_search"})
     */
    private $dayNumber;

    /**
     * @ORM\Column(type="time", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"product_search", "shop_search"})
     */
    private $morningStartAt;

    /**
     * @ORM\Column(type="time", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"product_search", "shop_search"})
     */
    private $morningEndAt;

    /**
     * @ORM\Column(type="time", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"product_search", "shop_search"})
     */
    private $afternoonStartAt;

    /**
     * @ORM\Column(type="time", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"product_search", "shop_search"})
     */
    private $afternoonEndAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop", inversedBy="openingTimes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shop;

    public function __toString()
    {
        return $this->getDay();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDay(): ?string
    {
        return $this->day;
    }

    public function setDay(string $day): self
    {
        $this->day = $day;

        return $this;
    }

    public function getMorningStartAt(): ?\DateTimeInterface
    {
        return $this->morningStartAt;
    }

    public function setMorningStartAt(?\DateTimeInterface $morningStartAt): self
    {
        $this->morningStartAt = $morningStartAt;

        return $this;
    }

    public function getMorningEndAt(): ?\DateTimeInterface
    {
        return $this->morningEndAt;
    }

    public function setMorningEndAt(?\DateTimeInterface $morningEndAt): self
    {
        $this->morningEndAt = $morningEndAt;

        return $this;
    }

    public function getAfternoonStartAt(): ?\DateTimeInterface
    {
        return $this->afternoonStartAt;
    }

    public function setAfternoonStartAt(?\DateTimeInterface $afternoonStartAt): self
    {
        $this->afternoonStartAt = $afternoonStartAt;

        return $this;
    }

    public function getAfternoonEndAt(): ?\DateTimeInterface
    {
        return $this->afternoonEndAt;
    }

    public function setAfternoonEndAt(?\DateTimeInterface $afternoonEndAt): self
    {
        $this->afternoonEndAt = $afternoonEndAt;

        return $this;
    }

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(?Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    public function getDayNumber(): ?int
    {
        return $this->dayNumber;
    }

    public function setDayNumber(int $dayNumber): self
    {
        $this->dayNumber = $dayNumber;

        return $this;
    }
}
