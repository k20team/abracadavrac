<?php

namespace App\Helper;

use App\Entity\GlobalSetting;
use Doctrine\ORM\EntityManagerInterface;
use \Mailjet\Resources;
use \Mailjet\Client as MailjetClient;
use Symfony\Component\Translation\TranslatorInterface;
use UserBundle\Entity\User;

class EmailHelper
{
    private $mailjetPublic;
    private $mailjetPrivate;
    private $deliveryAddress;

    private $mailer;
    private $transport;
    private $templating;
    private $em;

    public function __construct(
        string $mailjetPublic,
        string $mailjetPrivate,
        string $deliveryAddress,
        \Swift_Mailer $mailer,
        \Swift_Transport $transport,
        \Twig\Environment $templating,
        EntityManagerInterface $em
    ) {
        $this->mailjetPublic = $mailjetPublic;
        $this->mailjetPrivate = $mailjetPrivate;

        $this->deliveryAddress = $deliveryAddress;
        $this->mailer = $mailer;
        $this->transport = $transport;
        $this->templating = $templating;
        $this->em = $em;
    }

    private function getEmail($email) {
        if ($_SERVER['APP_ENV'] == 'dev') {
            $email = $this->deliveryAddress;
        }
        return $email;
    }

    public function sendFormContact($contact) {
        $this->sendMail("bonjour@abracada-vrac.com",
                        "bonjour@abracada-vrac.com",
                        'Demande information Abracadavrac',
                        $this->templating->render('emails/contact-mail.html.twig', ['contact' => $contact]),
                        $this->templating->render('emails/contact-mail.text.twig', ['contact' => $contact])
        );
        $this->sendMail("bonjour@abracada-vrac.com",
                        $contact['email'],
                        'Demande information Abracadavrac',
                        $this->templating->render('emails/contact-mail-receipt.html.twig', ['contact' => $contact]),
                        $this->templating->render('emails/contact-mail-receipt.text.twig', ['contact' => $contact])
        );
    }

    public function resetPassword($user, $url) {
        //$templateId = $this->em->getRepository(GlobalSetting::class)->findOneByName('MAILJET_TEMPLATE_RESET_PASWORD')->getValue();
        $result = $this->sendMail("ne-pas-repondre@abracada-vrac.com",
                            $user->getEmail(),
                            "Abracada-vrac - Réinitialisation de mot de passe",
                            $this->templating->render('emails/reset-password.html.twig', ['user' => $user, 'url' => $url]),
                            $this->templating->render('emails/reset-password.text.twig', ['user' => $user, 'url' => $url])
                        );
        return $result;
    }

    private function sendMail($from, $to, $subject, $content_html, $content_text)
    {
        $to = $this->getEmail($to);
        $mj = new MailjetClient($this->mailjetPublic, $this->mailjetPrivate, true, ['version' => 'v3.1']);
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => $from,
                        'Name' => "Abracada-vrac"
                    ],
                    'To' => [
                        [
                            'Email' => $to,
                            'Name' => ""
                        ]
                    ],
                    'Subject' => $subject,
                    'TextPart' => $content_text,
                    'HTMLPart' => $content_html
                ]
            ]
        ];
        $response = $mj->post(Resources::$Email, ['body' => $body]);
        return $response;
    }

    private function sendTemplate($templateId, $fromEmail, $fromName, $subject, $toEmail, $vars)
    {
        $toEmail = $this->getEmail($toEmail);
        $mj = new MailjetClient($this->mailjetPublic, $this->mailjetPrivate, true, ['version' => 'v3.1']);
        $body = [
            'Messages' => [
                [
                    'From' => [
                    'Email' => $fromEmail,
                    'Name' => $fromName
                    ],
                    'To' => [
                    [
                        'Email' => $toEmail,
                        'Name' => ""
                    ]
                    ],
                    'TemplateID' => (int) $templateId,
                    'TemplateLanguage' => true,
                    'Subject' => $subject,
                    'Variables' => $vars
                ]
            ]
        ];
        $response = $mj->post(Resources::$Email, ['body' => $body]);
        return $response;
    }

}
