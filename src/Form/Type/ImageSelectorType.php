<?php 
namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ImageSelectorType extends AbstractType
{
    public function getParent()
    {
        return EntityType::class;
    }

    
    public function getBlockPrefix()
    {
        return 'image_selector';
    }

}