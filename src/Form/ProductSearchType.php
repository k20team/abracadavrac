<?php
namespace App\Form;

use App\Entity\Label;
use App\Entity\ProductType;
use App\Entity\ProductUsage;
use App\Entity\Product;
use App\Form\Type\CheckButtonType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class ProductSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('search', TextType::class, array(
                'required' => false,
            ));
            
        if ($options['with_address']) {
            $builder
                ->add('address', TextType::class, array(
                    'required' => false,
                ))
                ->add('lng', HiddenType::class)
                ->add('lat', HiddenType::class)
                ->add('origin', HiddenType::class, array(
                    'data' => Product::ORIGIN_WORLD
                ));
        }
        if (!$options['partial']) {
            $builder
            ->add('types', CheckButtonType::class, array(
                'required' => false,
                'multiple' => true,
                'expanded' => true,
                'class' => ProductType::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->orderBy('t.appearanceOrder', 'ASC');
                },
            ))
            ->add('usages', CheckButtonType::class, array(
                'required' => false,
                'multiple' => true,
                'expanded' => true,
                'class' => ProductUsage::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.appearanceOrder', 'ASC');
                },
            ))
            ->add('labels', CheckButtonType::class, array(
                'required' => false,
                'multiple' => true,
                'expanded' => true,
                'class' => Label::class,
                'attr' => ['class' => 'button-style'],
                'choice_attr' => function ($label, $key, $value) {
                    return ['icon' => 'icon-' . strtolower($label->getIcon())];
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('l')
                        ->orderBy('l.id', 'ASC');
                },
            ))
            ->add('origin', ChoiceType::class, array(
                'required' => true,
                'multiple' => false,
                'expanded' => true,
                'choices' => [
                    'Monde' => Product::ORIGIN_WORLD,
                    'Europe' => Product::ORIGIN_EUROPE,
                    'France' => Product::ORIGIN_FRANCE,
                    'Région' => Product::ORIGIN_REGIONAL,
                ],
                'choice_attr' => function ($choice, $key, $value) {
                    return ['class' => 'attending-' . strtolower($choice)];
                },
                'data' => Product::ORIGIN_WORLD
            ))
            ;
        }

    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'partial' => false,
            'with_address' => true,
        ));
    }
}
