<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class ContactMessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, array(
                'required' => true,
            ))
            ->add('information', EmailType::class, array(
                'required' => true,
                'mapped' => 'email'
            ))
            ->add('text', TextareaType::class, array(
                'required' => true,
            ))
            ->add('requirement', CheckboxType::class, array(
                'required' => true,
                'mapped' => false
            ))
            ->add('email', TextType::class, array(
                'required' => false,
                'mapped' => false
            ));
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array());
    }
}
