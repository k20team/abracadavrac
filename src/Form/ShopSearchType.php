<?php
namespace App\Form;

use App\Entity\ShopType;
use App\Form\Type\CheckButtonType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class ShopSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('search', TextType::class)
            ->add('address', TextType::class)
            ->add('lng', HiddenType::class)
            ->add('lat', HiddenType::class)
            /*->add('types', CheckButtonType::class, array(
                'required' => false,
                'multiple' => true,
                'expanded' => true,
                'class' => ShopType::class
            ))*/;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
        ));
    }
}
