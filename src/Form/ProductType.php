<?php
namespace App\Form;

use App\Entity\Label;
use App\Entity\MeasureUnit;
use App\Entity\ProductType as EntityProductType;
use App\Entity\ProductUsage;
use App\Entity\Product;
use App\Entity\ProductImage;
use App\Form\Type\CheckButtonType;
use App\Form\Type\ImageSelectorType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class ProductType extends AbstractType
{
    private $entityManager;
    public function __construct(EntityManagerInterface $entityManager)
    {
        // 3. Update the value of the private entityManager variable through injection
        $this->entityManager = $entityManager;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, array(
                'required' => true,
            ))
            ->add('price', NumberType::class, array(
                'required' => true,
            ))
            ->add('unit', EntityType::class, [
                'label' => 'Unité',
                'class' => MeasureUnit::class,
                'choice_label' => 'shortName',
            ])
            ->add('origin', ChoiceType::class, array(
                'required' => true,
                'multiple' => false,
                'expanded' => true,
                'choices' => [
                    'Monde' => Product::ORIGIN_WORLD,
                    'Europe' => Product::ORIGIN_EUROPE,
                    'France' => Product::ORIGIN_FRANCE,
                    'Région' => Product::ORIGIN_REGIONAL,
                ],
                'choice_attr' => function ($choice, $key, $value) {
                    return ['class' => 'attending-' . strtolower($choice)];
                }
            ))
            ->add('type', CheckButtonType::class, array(
                'required' => true,
                'multiple' => false,
                'expanded' => true,
                'class' => EntityProductType::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->orderBy('t.appearanceOrder', 'ASC');
                },
            ))
            ->add('usages', CheckButtonType::class, array(
                'required' => false,
                'multiple' => true,
                'expanded' => true,
                'class' => ProductUsage::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.appearanceOrder', 'ASC');
                },
            ))
            ->add('labels', CheckButtonType::class, array(
                'required' => false,
                'multiple' => true,
                'expanded' => true,
                'class' => Label::class,
                'attr' => ['class' => 'button-style'],
                'choice_attr' => function ($label, $key, $value) {
                    return ['icon' => 'icon-' . strtolower($label->getIcon())];
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('l')
                        ->orderBy('l.id', 'ASC');
                },
            ))
            ->add('description', TextareaType::class, array(
                'required' => false,
            ))
            ->add('producer', TextType::class, array(
                'required' => false,
            ))
            ->add('productImage', ImageSelectorType::class, [
                'label' => 'Image',
                'required' => false,
                'class' => ProductImage::class,
                'choice_label' => 'name'
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
        ));
    }
}
