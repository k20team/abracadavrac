<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200504081307 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE address_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE label_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE measure_unit_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE opening_times_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_usage_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE shop_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE shop_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE static_pages_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE address (id INT NOT NULL, address VARCHAR(255) NOT NULL, lng DOUBLE PRECISION NOT NULL, lat DOUBLE PRECISION NOT NULL, location geography(POINT, 4326) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE label (id INT NOT NULL, name VARCHAR(128) NOT NULL, icon VARCHAR(31) DEFAULT NULL, description TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE measure_unit (id INT NOT NULL, name VARCHAR(63) NOT NULL, short_name VARCHAR(15) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE opening_times (id INT NOT NULL, shop_id INT NOT NULL, day VARCHAR(15) NOT NULL, day_number INT NOT NULL, morning_start_at TIME(0) WITHOUT TIME ZONE DEFAULT NULL, morning_end_at TIME(0) WITHOUT TIME ZONE DEFAULT NULL, afternoon_start_at TIME(0) WITHOUT TIME ZONE DEFAULT NULL, afternoon_end_at TIME(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B18D920A4D16C4DD ON opening_times (shop_id)');
        $this->addSql('CREATE TABLE product (id INT NOT NULL, unit_id INT NOT NULL, shop_id INT NOT NULL, type_id INT NOT NULL, name VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, origin VARCHAR(64) NOT NULL, producer VARCHAR(255) DEFAULT NULL, price DOUBLE PRECISION NOT NULL, price_updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, image_name VARCHAR(255) DEFAULT NULL, uploaded_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, is_published BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D34A04ADF8BD700D ON product (unit_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD4D16C4DD ON product (shop_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADC54C8C93 ON product (type_id)');
        $this->addSql('CREATE TABLE product_label (product_id INT NOT NULL, label_id INT NOT NULL, PRIMARY KEY(product_id, label_id))');
        $this->addSql('CREATE INDEX IDX_AFFB2BB44584665A ON product_label (product_id)');
        $this->addSql('CREATE INDEX IDX_AFFB2BB433B92F39 ON product_label (label_id)');
        $this->addSql('CREATE TABLE product_product_usage (product_id INT NOT NULL, product_usage_id INT NOT NULL, PRIMARY KEY(product_id, product_usage_id))');
        $this->addSql('CREATE INDEX IDX_AB5A39DE4584665A ON product_product_usage (product_id)');
        $this->addSql('CREATE INDEX IDX_AB5A39DEEAE08C0D ON product_product_usage (product_usage_id)');
        $this->addSql('CREATE TABLE product_type (id INT NOT NULL, name VARCHAR(128) NOT NULL, slug VARCHAR(128) NOT NULL, description TEXT DEFAULT NULL, appearance_order INT NOT NULL, icon VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1367588989D9B62 ON product_type (slug)');
        $this->addSql('CREATE TABLE product_usage (id INT NOT NULL, name VARCHAR(128) NOT NULL, slug VARCHAR(128) NOT NULL, description VARCHAR(255) DEFAULT NULL, appearance_order INT NOT NULL, cover_name VARCHAR(255) DEFAULT NULL, uploaded_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_71B7252C989D9B62 ON product_usage (slug)');
        $this->addSql('CREATE TABLE shop (id INT NOT NULL, address_id INT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(128) NOT NULL, description TEXT DEFAULT NULL, phone VARCHAR(15) DEFAULT NULL, website VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, is_published BOOLEAN NOT NULL, logo_name VARCHAR(255) DEFAULT NULL, uploaded_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AC6A4CA2989D9B62 ON shop (slug)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AC6A4CA2F5B7AF75 ON shop (address_id)');
        $this->addSql('CREATE TABLE shop_type (id INT NOT NULL, name VARCHAR(128) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE shop_type_shop (shop_type_id INT NOT NULL, shop_id INT NOT NULL, PRIMARY KEY(shop_type_id, shop_id))');
        $this->addSql('CREATE INDEX IDX_62D20721C67FCCB9 ON shop_type_shop (shop_type_id)');
        $this->addSql('CREATE INDEX IDX_62D207214D16C4DD ON shop_type_shop (shop_id)');
        $this->addSql('CREATE TABLE static_pages (id INT NOT NULL, title VARCHAR(67) NOT NULL, slug VARCHAR(67) NOT NULL, content TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE user_user (id INT NOT NULL, email VARCHAR(255) NOT NULL, firstname VARCHAR(65) NOT NULL, lastname VARCHAR(65) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, is_enabled BOOLEAN NOT NULL, is_test BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE opening_times ADD CONSTRAINT FK_B18D920A4D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADF8BD700D FOREIGN KEY (unit_id) REFERENCES measure_unit (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD4D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADC54C8C93 FOREIGN KEY (type_id) REFERENCES product_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_label ADD CONSTRAINT FK_AFFB2BB44584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_label ADD CONSTRAINT FK_AFFB2BB433B92F39 FOREIGN KEY (label_id) REFERENCES label (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_product_usage ADD CONSTRAINT FK_AB5A39DE4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_product_usage ADD CONSTRAINT FK_AB5A39DEEAE08C0D FOREIGN KEY (product_usage_id) REFERENCES product_usage (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE shop ADD CONSTRAINT FK_AC6A4CA2F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE shop_type_shop ADD CONSTRAINT FK_62D20721C67FCCB9 FOREIGN KEY (shop_type_id) REFERENCES shop_type (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE shop_type_shop ADD CONSTRAINT FK_62D207214D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE shop DROP CONSTRAINT FK_AC6A4CA2F5B7AF75');
        $this->addSql('ALTER TABLE product_label DROP CONSTRAINT FK_AFFB2BB433B92F39');
        $this->addSql('ALTER TABLE product DROP CONSTRAINT FK_D34A04ADF8BD700D');
        $this->addSql('ALTER TABLE product_label DROP CONSTRAINT FK_AFFB2BB44584665A');
        $this->addSql('ALTER TABLE product_product_usage DROP CONSTRAINT FK_AB5A39DE4584665A');
        $this->addSql('ALTER TABLE product DROP CONSTRAINT FK_D34A04ADC54C8C93');
        $this->addSql('ALTER TABLE product_product_usage DROP CONSTRAINT FK_AB5A39DEEAE08C0D');
        $this->addSql('ALTER TABLE opening_times DROP CONSTRAINT FK_B18D920A4D16C4DD');
        $this->addSql('ALTER TABLE product DROP CONSTRAINT FK_D34A04AD4D16C4DD');
        $this->addSql('ALTER TABLE shop_type_shop DROP CONSTRAINT FK_62D207214D16C4DD');
        $this->addSql('ALTER TABLE shop_type_shop DROP CONSTRAINT FK_62D20721C67FCCB9');
        $this->addSql('DROP SEQUENCE address_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE label_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE measure_unit_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE opening_times_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_usage_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE shop_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE shop_type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE static_pages_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE user_user_id_seq CASCADE');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE label');
        $this->addSql('DROP TABLE measure_unit');
        $this->addSql('DROP TABLE opening_times');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_label');
        $this->addSql('DROP TABLE product_product_usage');
        $this->addSql('DROP TABLE product_type');
        $this->addSql('DROP TABLE product_usage');
        $this->addSql('DROP TABLE shop');
        $this->addSql('DROP TABLE shop_type');
        $this->addSql('DROP TABLE shop_type_shop');
        $this->addSql('DROP TABLE static_pages');
        $this->addSql('DROP TABLE user_user');
    }
}
