<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200703085856 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE contact_message_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_image_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_signal_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE contact_message (id INT NOT NULL, name VARCHAR(255) NOT NULL, email TEXT NOT NULL, text TEXT NOT NULL, state VARCHAR(255) NOT NULL, sended_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE product_image (id INT NOT NULL, product_type_id INT NOT NULL, name VARCHAR(255) NOT NULL, image_name VARCHAR(255) NOT NULL, uploaded_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_64617F0314959723 ON product_image (product_type_id)');
        $this->addSql('CREATE TABLE product_signal (id INT NOT NULL, product_id INT NOT NULL, corrected_by_id INT DEFAULT NULL, type VARCHAR(31) NOT NULL, signaled_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, status VARCHAR(31) NOT NULL, corrected_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1670D4514584665A ON product_signal (product_id)');
        $this->addSql('CREATE INDEX IDX_1670D4518CF37DC4 ON product_signal (corrected_by_id)');
        $this->addSql('ALTER TABLE product_image ADD CONSTRAINT FK_64617F0314959723 FOREIGN KEY (product_type_id) REFERENCES product_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_signal ADD CONSTRAINT FK_1670D4514584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_signal ADD CONSTRAINT FK_1670D4518CF37DC4 FOREIGN KEY (corrected_by_id) REFERENCES user_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product ADD product_image_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product DROP image_name');
        $this->addSql('ALTER TABLE product DROP uploaded_at');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADF6154FFA FOREIGN KEY (product_image_id) REFERENCES product_image (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D34A04ADF6154FFA ON product (product_image_id)');
        $this->addSql('ALTER TABLE user_user ADD reset_password VARCHAR(255) DEFAULT NULL');
        $this->addSql('CREATE TABLE shop_user (shop_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(shop_id, user_id))');
        $this->addSql('CREATE INDEX IDX_4C6130324D16C4DD ON shop_user (shop_id)');
        $this->addSql('CREATE INDEX IDX_4C613032A76ED395 ON shop_user (user_id)');
        $this->addSql('ALTER TABLE shop_user ADD CONSTRAINT FK_4C6130324D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE shop_user ADD CONSTRAINT FK_4C613032A76ED395 FOREIGN KEY (user_id) REFERENCES user_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE product DROP CONSTRAINT FK_D34A04ADF6154FFA');
        $this->addSql('DROP SEQUENCE contact_message_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_image_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_signal_id_seq CASCADE');
        $this->addSql('DROP TABLE contact_message');
        $this->addSql('DROP TABLE product_image');
        $this->addSql('DROP TABLE product_signal');
        $this->addSql('ALTER TABLE user_user DROP reset_password');
        $this->addSql('DROP INDEX IDX_D34A04ADF6154FFA');
        $this->addSql('ALTER TABLE product ADD image_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD uploaded_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE product DROP product_image_id');
        $this->addSql('DROP TABLE shop_user');
    }
}
