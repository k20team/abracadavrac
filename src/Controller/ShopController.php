<?php

namespace App\Controller;

use App\Entity\Label;
use App\Entity\ProductType;
use App\Entity\ProductUsage;
use App\Entity\Shop;
use App\Entity\ShopType;
use App\Form\ProductSearchType;
use App\Form\ShopSearchType;
use App\Repository\ProductRepository;
use App\Repository\ProductTypeRepository;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

use App\Repository\ShopRepository;
use App\Services\GeoK2;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;

class ShopController extends AbstractController
{
    /**
     * @Route("/rechercher-un-magasin",
     *      name="search_shop",
     *      options={"expose"=true})
     * @Template()
     */
    public function search(Request $request,
                            ShopRepository $shopRepository,
                            GeoK2 $geoK2) {

        $form = $this->createForm(ShopSearchType::class, null, array(
            'csrf_protection' => false,
            'method' => 'GET',
        ));
        $form->handleRequest($request);
        $from = false;
        $resultsNumber = 60;
        $lng_default = 4.8332901;
        $lat_default = 45.7579386;

        if ($form->isSubmitted() && $form->isValid()) {
            [$shopList, $totalNumber, $lng, $lat] = $this->getShops($request, $form, $geoK2, $shopRepository, $resultsNumber);
            if ($lat !== null && $lng !== null) {
                $from = true;
            } else {
                $lng = $lng_default;
                $lat = $lat_default;
            }

            //--Si on a fait une demande en ajax
            if ($request->query->get('ajax') == '1') {
                $serializer = SerializerBuilder::create()->build();
                $json = $serializer->serialize([$shopList, $totalNumber, $lng, $lat], 'json', SerializationContext::create()->setGroups(array('shop_search')));
                return new JsonResponse($json, 200, [], true);
            }
        } else {
            [$shopList, $totalNumber] = $shopRepository->search("", 0, $resultsNumber);
            $lng = $lng_default;
            $lat = $lat_default;
        }
        $searchForm = $form->createView();

        $serializer = SerializerBuilder::create()->build();
        $shopList = $serializer->serialize($shopList, 'json', SerializationContext::create()->setGroups(array('shop_search')));
        return compact('searchForm', 'lat', 'lng', 'shopList', 'totalNumber', 'from', 'resultsNumber');
    }

    private function getShops($request, $form, $geoK2, $shopRepository, $resultsNumber) {
        $search = (String) $form['search']->getData();
        $lat = $form['lat']->getData();
        $lng = $form['lng']->getData();
        $address = $form['address']->getData();

        //--Ne font pas parti du formulaire, sont donc ajouté a la requete
        $startLimit = $request->query->get('startLimit');
        if ($startLimit == null) {
            $startLimit = 0;
        }
        $minLat = $request->query->get('minLat');
        $minLng = $request->query->get('minLng');
        $maxLat = $request->query->get('maxLat');
        $maxLng = $request->query->get('maxLng');

        //--Si l'utilisateur à simplement saisie une adresse sans la valider, on va quand même l'utiliser
        if ($lng === null && $lat === null && $address !== null) {
            $bestLocation = $geoK2->getBestSimpleLocation($address);
            $lng = $bestLocation->getLng();
            $lat = $bestLocation->getLat();
        }

        [$shopList, $totalNumber] = $shopRepository->search($search, $startLimit, $resultsNumber, $lng, $lat, $minLat, $minLng, $maxLat, $maxLng);
        //[$productList, $totalNumber] = $productRepository->search($search, $types, $usages, $labels, $origin, $startLimit, $lng, $lat, $minLat, $minLng, $maxLat, $maxLng);
        return [$shopList, $totalNumber, $lng, $lat];
    }

    /**
     * @Route("/magasin/{slug}", name="shop_show", options={"expose"=true})
     * @Template()
     */
    public function show(
        Request $request,
        ShopRepository $shopRepository,
        ProductRepository $productRepository,
        ProductTypeRepository $productTypeRepository,
        string $slug
    ) {

        $shop = $shopRepository->findOneBySlug($slug);

        $form = $this->createForm(ProductSearchType::class, null, array(
            'partial' => false,
            'with_address' => false,
            'csrf_protection' => false,
            'method' => 'GET',
        ));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $search = (String) $form['search']->getData();
            $types_id = $form['types']->getData();
            $usages_id = $form['usages']->getData();
            $labels_id = $form['labels']->getData();
            $origin = $form['origin']->getData();

            $types = $usages = $labels = null;
            foreach ($types_id as $id) {
                $types[] = $this->getDoctrine()->getRepository(ProductType::class)->find($id);
            }
            foreach ($usages_id as $id) {
                $usages[] = $this->getDoctrine()->getRepository(ProductUsage::class)->find($id);
            }
            foreach ($labels_id as $id) {
                $labels[] = $this->getDoctrine()->getRepository(Label::class)->find($id);
            }
            $productList = $productRepository->searchInShop($shop, $search, $types, $usages, $labels, $origin);

            $json = SerializerBuilder::create()->build()->serialize($productList, 'json', SerializationContext::create()->setGroups(array('product_search')));
            return new JsonResponse($json, 200, [], true);
        }

        $productList = $productRepository->searchInShop($shop);
        $isManagedByUser = false;
        if ($shop->getManagedBy()->contains($this->getUser())) {
            $isManagedByUser = true;
        }
        $productShopList = SerializerBuilder::create()->build()->serialize($productList, 'json', SerializationContext::create()->setGroups(array('product_search')));
        $orderedCategories = SerializerBuilder::create()->build()->serialize($productTypeRepository->findAll(), 'json');
        $searchForm = $form->createView();
        return compact('shop', 'productShopList', 'searchForm', 'isManagedByUser', 'orderedCategories');
    }
}
