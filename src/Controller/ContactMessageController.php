<?php

namespace App\Controller;

use App\Entity\ContactMessage;
use App\Form\ContactMessageType;
use App\Helper\EmailHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class ContactMessageController extends AbstractController
{
    /**
     * @Route("/nous-contacter", name="contact_message")
     * @Template("contact/contact.html.twig")
     */
    public function contact(Request $request, EmailHelper $emailHelper) {
        $form = $this->createForm(ContactMessageType::class, null, array(
            'method' => 'POST',
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (empty($request->request->get('contact_message')['email'])) {

                $contact = new ContactMessage();
                $email = $request->request->get('contact_message')['information'];
                $name = $request->request->get('contact_message')['name'];
                $text = $request->request->get('contact_message')['text'];

                $contact->setName($name);
                $contact->setEmail($email);
                $contact->setText($text);

                $emailHelper->sendFormContact(["name" => $contact->getName(), "email" => $contact->getEmail(), "text" => $contact->getText()]);

                $this->getDoctrine()->getManager()->persist($contact);
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash('success', 'Votre message a été transmis, nous vous répondrons dans les meilleurs délais.'); // Permet un message flash de renvoi

            }
        }
        $contactForm = $form->createView();
        return compact('contactForm');

    }


}
