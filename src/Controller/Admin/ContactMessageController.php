<?php

namespace App\Controller\Admin;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Entity\ContactMessage;

class ContactMessageController extends CRUDController
{
    public function treateMessageAction()
    {
        $object = $this->admin->getSubject();
        if (!$object) {
            throw new NotFoundHttpException(sprintf('Message introuvable'));
        }

        $object->setState(ContactMessage::STATUS_DONE);
        $this->admin->update($object);

        $this->addFlash('sonata_flash_success', sprintf('Le message est indiqué comme traité !', $object->getEmail()));

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    public function untreateMessageAction()
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('Message introuvable'));
        }

        $object->setState(ContactMessage::STATUS_WAITING);
        $this->admin->update($object);

        $this->addFlash('sonata_flash_success', sprintf('Le message est indiqué comme non traité !', $object->getEmail()));

        return new RedirectResponse($this->admin->generateUrl('list'));
    }
}