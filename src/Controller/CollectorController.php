<?php

namespace App\Controller;

use App\Form\ProductSearchType;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use App\Repository\ProductTypeRepository;
use App\Repository\ProductUsageRepository;
use App\Repository\ShopRepository;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

use App\Repository\StaticPagesRepository;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;

class CollectorController extends AbstractController
{
    /**
     * @Route("/espace-collecteurs", name="collector_home")
     * @Template()
     */
    public function home(Request $request, 
                        ShopRepository $shopRepository, 
                        ProductRepository $productRepository) {
        $user = $this->getUser();
        $shops = $user->getManagedShops();
        $products = [];
        foreach ($shops as $shop) {
            $products[$shop->getId()] = $productRepository->getMoreSignaledProductsInShop($shop);
        }
        $serializer = SerializerBuilder::create()->build();
        $shops = $serializer->serialize($shops, 'json', SerializationContext::create()->setGroups(array('shop_search')));
        $products = $serializer->serialize($products, 'json', SerializationContext::create()->setGroups(array('product_search')));
        return compact('shops', 'products');
    }
}
