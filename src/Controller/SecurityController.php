<?php
namespace App\Controller;

use App\Entity\User;
use App\Form\ChangePasswordType;
use App\Form\ForgottenPasswordType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\LoginForm;
use App\Form\ResetPasswordType;
use App\Helper\EmailHelper;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

final class SecurityController extends AbstractController
{
    /**
     * @var AuthenticationUtils
     */
    private $authenticationUtils;

    public function __construct(AuthenticationUtils $authenticationUtils)
    {
        $this->authenticationUtils = $authenticationUtils;
    }

    /**
     * @Route("/connexion", name="login")
     */
    public function loginAction(): Response
    {
        $form = $this->createForm(LoginForm::class, [
            'email' => $this->authenticationUtils->getLastUsername()
        ]);

        return $this->render('security/login.html.twig', [
            'last_username' => $this->authenticationUtils->getLastUsername(),
            'form' => $form->createView(),
            'error' => $this->authenticationUtils->getLastAuthenticationError(),
        ]);
    }

    /**
     * @Route("/deconnexion", name="logout")
     */
    public function logoutAction(): void
    {
        // Left empty intentionally because this will be handled by Symfony.
    }
    /**
     * @Route("/mot-de-passe-oublie", name="forgotten_password")
     */
    public function resetPassword(Request $request, EmailHelper $emailHelper)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(ForgottenPasswordType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $entityManager->getRepository(User::class)->findOneByEmail($form->getData()['email']);
            if ($user !== null) {
                $token = uniqid();
                $user->setResetPassword($token);
                $entityManager->persist($user);
                $entityManager->flush();
                $emailHelper->resetPassword($user, $this->generateUrl('reset_password', ['token'=> $token], UrlGeneratorInterface::ABSOLUTE_URL));

                return $this->render('security/forgotten-password-confirmation.html.twig');
            }
            else {
                return $this->render('security/forgotten-password.html.twig', array(
                    'form' => $form->createView(),
                    'error' => 'unrecognize_email',
                ));
            }
        }

        return $this->render('security/forgotten-password.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    
    /**
     * @Route("/reinitialisation-de-mot-de-passe", name="reset_password")
     */
    public function resetPasswordToken(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $token = $request->query->get('token');
        if ($token !== null) {
            $entityManager = $this->getDoctrine()->getManager();
            $user = $entityManager->getRepository(User::class)->findOneByResetPassword($token);
            if ($user !== null) {
                $form = $this->createForm(ResetPasswordType::class);

                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {
                    $plainPassword = $form->getData()['password'];
                    $encoded = $encoder->encodePassword($user, $plainPassword);
                    $user->setPassword($encoded);
                    $user->setResetPassword(null);
                    $entityManager->persist($user);
                    $entityManager->flush();

                    return $this->render('security/reset-password-confirmation.html.twig');
                }

                return $this->render('security/reset-password.html.twig', array(
                    'form' => $form->createView(),
                    'token' => $token,
                ));
            }
        }
        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/changement-de-mot-de-passe", name="change_password")
     */
    public function changePasswordToken(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $form = $this->createForm(ChangePasswordType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $oldPassword = $form->getData()['oldPassword'];
            if (!$encoder->isPasswordValid($user, $oldPassword)) {
                return $this->render('security/change-password.html.twig', array(
                    'form' => $form->createView(),
                    'error' => 'unrecognize_password'
                ));
            }
            $newPassword = $form->getData()['newPassword'];
            $encoded = $encoder->encodePassword($user, $newPassword);
            $user->setPassword($encoded);
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('profile_profile');
        }

        return $this->render('security/change-password.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/access-interdit/{message}", name="access_denied")
     */
    public function accessDenied(Request $request, string $message)
    {
        return $this->render('error/access-denied.html.twig', array('message' => $message));
    }
}