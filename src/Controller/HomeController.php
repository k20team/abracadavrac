<?php

namespace App\Controller;

use App\Form\ProductSearchType;
use App\Repository\ProductTypeRepository;
use App\Repository\ProductUsageRepository;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

use App\Repository\StaticPagesRepository;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     * @Template()
     */
    public function home(Request $request, 
                        StaticPagesRepository $staticPagesRepository, 
                        ProductUsageRepository $productUsageRepository, 
                        ProductTypeRepository $productTypeRepository) {

        $form = $this->createForm(ProductSearchType::class, null, array(
            /*'action' => $this->generateUrl('search_product'),*/
            'method' => 'GET',
            'partial' => true,
            'with_address' => true,
            'csrf_protection' => false
        ));

        $productUsages = $productUsageRepository->findAll();
        $productTypes = $productTypeRepository->findAll();
        $staticPages = $staticPagesRepository->findAll();
        $searchForm = $form->createView();
        return compact('searchForm', 'staticPages', 'productUsages', 'productTypes');
    }
}
