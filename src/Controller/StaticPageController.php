<?php

namespace App\Controller;

use App\Repository\LabelRepository;
use App\Repository\ProductTypeRepository;
use App\Repository\ProductUsageRepository;
use App\Repository\ShopRepository;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

use App\Repository\StaticPagesRepository;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class StaticPageController extends AbstractController
{
    /**
     * @Route("/page/{slug}", name="static_page_show")
     * @Template()
     */
    public function show(
                Request $request,
                StaticPagesRepository $staticPagesRepository,
                String $slug ) {

        $page = $staticPagesRepository->findOneBySlug($slug);

        return compact('page');
    }

    /**
     * @Route("/abracadaquoi/c-est-quoi-le-vrac", name="static_page_abracadaquoi")
     * @Template()
     */
    public function abracadaquoi(Request $request) {

        //$page = $staticPagesRepository->findOneBySlug($slug);

        return []; //compact('page');
    }

    /**
     * @Route("/abracadaquoi/nos-choix", name="static_page_noschoix")
     * @Template()
     */
    public function noschoix(Request $request) {

        //$page = $staticPagesRepository->findOneBySlug($slug);

        return []; //compact('page');
    }

    /**
     * @Route("/abracadaquoi/les-labels", name="static_page_labels")
     * @Template()
     */
    public function labels(Request $request, LabelRepository $labelRepository) {

        $labels = $labelRepository->findAll();

        return compact('labels');
    }

    /**
     * @Route("/plan-du-site", name="static_page_sitemap")
     * @Template()
     */
    public function sitemap(Request $request,
                            StaticPagesRepository $staticPagesRepository,
                            ProductTypeRepository $productTypeRepository,
                            ProductUsageRepository $productUsageRepository,
                            ShopRepository $shopRepository) {

        $mains['Page d\'accueil'] = $this->generateUrl('homepage', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $mains["C'est quoi le vrac"] = $this->generateUrl('static_page_abracadaquoi', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $mains["Nos choix"] = $this->generateUrl('static_page_noschoix', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $mains["Les labels"] = $this->generateUrl('static_page_labels', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $mains["Rechercher un produit"] = $this->generateUrl('search_product', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $mains["Rechercher un magasin"] = $this->generateUrl('search_shop', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $mains["Participer au projet"] = $this->generateUrl('contact_message', [], UrlGeneratorInterface::ABSOLUTE_URL);

        //--Page statiques database
        $staticpages = $staticPagesRepository->findAll();
        foreach ($staticpages as $staticpage) {
            $mains[$staticpage->getTitle()] = $this->generateUrl('static_page_show', ['slug' => $staticpage->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
        }
        $mains["Contact"] = $this->generateUrl('contact_message', [], UrlGeneratorInterface::ABSOLUTE_URL);

        //--Catégories
        $categoriesObjects = $productTypeRepository->findAll();
        foreach ($categoriesObjects as $category) {
            $categories[$category->getName()] = $this->generateUrl('search_product_by_category', ['slug' => $category->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
        }

        //--Catégories
        $usagesObjects = $productUsageRepository->findAll();
        foreach ($usagesObjects as $usage) {
            $usages[$usage->getName()] = $this->generateUrl('search_product_by_usage', ['slug' => $usage->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
        }

        //--Catégories
        $shopsObjects = $shopRepository->findAllActive();
        foreach ($shopsObjects as $shop) {
            $shops[$shop->getName()] = $this->generateUrl('shop_show', ['slug' => $shop->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
        }

        $this->sitemapXmlGenerator($mains, $categories, $usages, $shops);
        return compact('mains', 'categories', 'usages', 'shops');
    }

    private function sitemapXmlGenerator($mains, $categories, $usages, $shops) {
        $content = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="'.$this->generateUrl('homepage', [], UrlGeneratorInterface::ABSOLUTE_URL).'/sitemap.xml">';
        foreach ($mains as $url) {
            $content = $content."<url><loc>".$url."</loc><changefreq>monthly</changefreq></url>";
        }
        foreach ($categories as $url) {
            $content = $content."<url><loc>".$url."</loc><changefreq>weekly</changefreq></url>";
        }
        foreach ($usages as $url) {
            $content = $content."<url><loc>".$url."</loc><changefreq>weekly</changefreq></url>";
        }
        foreach ($shops as $url) {
            $content = $content."<url><loc>".$url."</loc><changefreq>weekly</changefreq></url>";
        }

        $content = $content."</urlset>";

        $fichier = fopen("sitemap.xml","w");
        fwrite($fichier,$content);
        fclose($fichier);

    }
}
