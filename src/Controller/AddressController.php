<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializerBuilder;
use App\Services\GeoK2;

class AddressController extends AbstractController
{
    /**
     * @Route("/autocomplete-address/{address}", name="search_autocomplete_address", options={"expose"=true})
     * @Template()
     */
    public function autocompleteAddress(GeoK2 $geoK2, string $address) {
        $addresses = $geoK2->getSimpleLocation($address);
        $serializer = SerializerBuilder::create()->build();
        $json = $serializer->serialize($addresses, 'json');
        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("/get-address-from-location/{lng}/{lat}", name="search_address_from_location", options={"expose"=true})
     * @Template()
     */
    public function getAddressFromLocation(GeoK2 $geoK2, float $lng, float $lat) {

        $address = $geoK2->getBestSimpleAddress([$lng, $lat]);
        $serializer = SerializerBuilder::create()->build();
        $json = $serializer->serialize($address, 'json');
        return new JsonResponse($json, 200, [], true);
    }

}