<?php

namespace App\Controller;

use App\Entity\Label;
use App\Entity\MeasureUnit;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use App\Entity\Product;
use App\Entity\ProductImage;
use App\Entity\ProductSignal;
use App\Repository\ProductRepository;
use App\Entity\ProductType;
use App\Entity\ProductUsage;
use App\Entity\Shop;
use App\Form\ProductSearchType;
use App\Form\ProductType as FormProductType;
use App\Repository\ProductImageRepository;
use App\Repository\ProductTypeRepository;
use App\Repository\ProductUsageRepository;
use App\Repository\ShopRepository;
use App\Services\GeoK2;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ProductController extends AbstractController
{
    /**
     * @Route("/rechercher-un-produit",
     *      name="search_product",
     *      options={"expose"=true})
     * @Template()
     */
    public function search(Request $request,
                           ProductRepository $productRepository,
                           GeoK2 $geoK2) {

        $form = $this->createForm(ProductSearchType::class, null, array(
            'partial' => false,
            'with_address' => true,
            'csrf_protection' => false,
            'method' => 'GET',
        ));
        $form->handleRequest($request);
        $from = false;
        $resultsNumber = 60;
        $lng_default = 4.8332901;
        $lat_default = 45.7579386;

        if ($form->isSubmitted() && $form->isValid()) {
            [$productList, $totalNumber, $lng, $lat] = $this->getProducts($request, $form, $geoK2, $productRepository, $resultsNumber);
            if ($lat !== null && $lng !== null) {
                $from = true;
            } else {
                $lng = $lng_default;
                $lat = $lat_default;
            }

            //--Si on a fait une demande en ajax
            if ($request->query->get('ajax') == '1') {
                $serializer = SerializerBuilder::create()->build();
                $json = $serializer->serialize([$productList, $totalNumber, $lng, $lat, $from], 'json', SerializationContext::create()->setGroups(array('product_search')));
                return new JsonResponse($json, 200, [], true);
            }
        } else {
            [$productList, $totalNumber] = $productRepository->search("", null, null, null, Product::ORIGIN_WORLD, 0, $resultsNumber);
            $lng = $lng_default;
            $lat = $lat_default;
        }
        $searchForm = $form->createView();
        $serializer = SerializerBuilder::create()->build();
        $productList = $serializer->serialize($productList, 'json', SerializationContext::create()->setGroups(array('product_search')));
        
        $denyRobots = true;

        return compact('searchForm', 'lat', 'lng', 'productList', 'totalNumber', 'from', 'resultsNumber', 'denyRobots');
    }

    private function getProducts($request, $form, $geoK2, $productRepository, $resultsNumber) {
        $search = (String) $form['search']->getData();
        $lat = $form['lat']->getData();
        $lng = $form['lng']->getData();
        $address = $form['address']->getData();
        $types_id = $form['types']->getData();
        $usages_id = $form['usages']->getData();
        $labels_id = $form['labels']->getData();
        $origin = $form['origin']->getData();

        //--Ne font pas parti du formulaire, sont donc ajouté a la requete
        $startLimit = $request->query->get('startLimit');
        if ($startLimit == null) {
            $startLimit = 0;
        }
        $minLat = $request->query->get('minLat');
        $minLng = $request->query->get('minLng');
        $maxLat = $request->query->get('maxLat');
        $maxLng = $request->query->get('maxLng');

        //--Si l'utilisateur à simplement saisie une adresse sans la valider, on va quand même l'utiliser
        if ($lng === null && $lat === null && $address !== null) {
            $bestLocation = $geoK2->getBestSimpleLocation($address);
            $lng = $bestLocation->getLng();
            $lat = $bestLocation->getLat();
        }

        $types = $usages = $labels = null;
        foreach ($types_id as $id) {
            $types[] = $this->getDoctrine()->getRepository(ProductType::class)->find($id);
        }
        foreach ($usages_id as $id) {
            $usages[] = $this->getDoctrine()->getRepository(ProductUsage::class)->find($id);
        }
        foreach ($labels_id as $id) {
            $labels[] = $this->getDoctrine()->getRepository(Label::class)->find($id);
        }
        [$productList, $totalNumber] = $productRepository->search($search, $types, $usages, $labels, $origin, $startLimit, $resultsNumber, $lng, $lat, $minLat, $minLng, $maxLat, $maxLng);
        return [$productList, $totalNumber, $lng, $lat];
    }

    /**
     * @Route("/rechercher-un-produit-par-categorie/{slug}",
     *      name="search_product_by_category",
     *      options={"expose"=true})
     * @Template("product/search.html.twig")
     */
    public function searchByCategory(Request $request,
                           ProductRepository $productRepository,
                           ProductTypeRepository $productTypeRepository,
                           GeoK2 $geoK2,
                           string $slug) {

        $form = $this->createForm(ProductSearchType::class, null, array(
            'partial' => false,
            'csrf_protection' => false,
            'method' => 'GET',
        ));
        $resultsNumber = 60;
        $selectedType = $productTypeRepository->findOneBySlug($slug);
        $from = false;
        [$productList, $totalNumber] = $productRepository->search("", [$selectedType], null, null, Product::ORIGIN_WORLD, 0, $resultsNumber);
        $lng = 4.8332901;
        $lat = 45.7579386;

        $form['types']->setData([$selectedType]);
        $searchForm = $form->createView();

        $serializer = SerializerBuilder::create()->build();
        $productList = $serializer->serialize($productList, 'json', SerializationContext::create()->setGroups(array('product_search')));
        return compact('searchForm', 'lat', 'lng', 'productList', 'totalNumber', 'from', 'selectedType', 'resultsNumber');
    }

    /**
     * @Route("/rechercher-un-produit-par-usage/{slug}",
     *      name="search_product_by_usage",
     *      options={"expose"=true})
     * @Template("product/search.html.twig")
     */
    public function searchByUsage(Request $request,
                           ProductRepository $productRepository,
                           ProductUsageRepository $productUsageRepository,
                           GeoK2 $geoK2,
                           string $slug) {

        $form = $this->createForm(ProductSearchType::class, null, array(
            'partial' => false,
            'csrf_protection' => false,
            'method' => 'GET',
        ));
        $resultsNumber = 60;
        $selectedUsage = $productUsageRepository->findOneBySlug($slug);
        $from = false;
        [$productList, $totalNumber] = $productRepository->search("", null, [$selectedUsage], null, Product::ORIGIN_WORLD, 0, $resultsNumber);
        $lng = 4.8332901;
        $lat = 45.7579386;

        $form['usages']->setData([$selectedUsage]);
        $searchForm = $form->createView();

        $serializer = SerializerBuilder::create()->build();
        $productList = $serializer->serialize($productList, 'json', SerializationContext::create()->setGroups(array('product_search')));
        return compact('searchForm', 'lat', 'lng', 'productList', 'totalNumber', 'from', 'selectedUsage', 'resultsNumber');
    }

    /**
     * @Route("/signaler-un-produit/{product}/price-{price}/stock-{stock}/{description}",
     *      defaults={
     *          "price": 0,
     *          "stock": 0,
     *      },
     *      name="signal_product",
     *      options={"expose"=true})
     * @Template()
     */
    public function signalProduct(Request $request,
                                Product $product,
                                bool $price = false,
                                bool $stock = false,
                                string $description = "")
    {
        if ($price) {
            $signal = new ProductSignal();
            $signal->setProduct($product);
            $signal->setType(ProductSignal::TYPE_PRICE);
            $signal->setComment($description);
            $this->getDoctrine()->getManager()->persist($signal);
        }
        if ($stock) {
            $signal = new ProductSignal();
            $signal->setProduct($product);
            $signal->setType(ProductSignal::TYPE_STOCK);
            $signal->setComment($description);
            $this->getDoctrine()->getManager()->persist($signal);
        }
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse("ok", 200, [], true);
    }

    /**
     * @Route("/creer-produit/{slug}/selected-cat-{productType}",
     *      defaults={
     *          "productType": 0,
     *      },
     * name="create_product",
     * methods={"GET","POST"},
     * options={"expose"=true})
     * @Template("product/new.html.twig")
     */
    public function new(Request $request, string $slug, ProductType $productType = null, ShopRepository $shopRepository, ProductImageRepository $productImageRepository)
    {
        $shop = $shopRepository->findOneBySlug($slug);
        $product = new Product();
        $product->setUnit($this->getDoctrine()->getManager()->getRepository(MeasureUnit::class)->findOneByShortName('kg'));
        if ($productType !== null) {
            $product->setType($productType);
        } else {
            $product->setType($this->getDoctrine()->getManager()->getRepository(ProductType::class)->findAll()[0]);
        }
        $form = $this->createForm(FormProductType::class, $product)
            ->add('saveAndAdd', SubmitType::class, ['label' => 'Enregistrer et ajouter un nouveau produit'])
            ->add('saveDashboard', SubmitType::class, ['label' => 'Enregistrer et retourner au tableau de bord'])
            ->add('saveShop', SubmitType::class, ['label' => 'Enregistrer et retourner au magasin']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($product->getUsages() as $usage) {
                $usage->addProduct($product);
            }
            foreach ($product->getLabels() as $label) {
                $label->addProduct($product);
            }
            $product->setShop($shop);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

            if ($form->get('saveAndAdd')->isClicked()) {
                return $this->redirectToRoute('create_product', ['slug' => $shop->getSlug(), 'productType' => $product->getType()->getId()]);
            } else if ($form->get('saveDashboard')->isClicked()) {
                return $this->redirectToRoute('collector_home');
            } else if ($form->get('saveShop')->isClicked()) {
                return $this->redirectToRoute('shop_show', ['slug' => $shop->getSlug()]);
            }
        }

        $selectedCategory = $product->getType();
        $productImages = $productImageRepository->findAll();
        $productImages = SerializerBuilder::create()->build()->serialize($productImages, 'json', SerializationContext::create()->setGroups(array('product_edit')));
        $form = $form->createView();
        return compact('product', 'shop', 'form', 'productImages', 'selectedCategory');
    }

    /**
     * @Route("/editer-produit/{product}",
     * name="edit_product",
     * methods={"GET","POST"},
     * options={"expose"=true})
     * @Template("product/edit.html.twig")
     */
    public function edit(Request $request, Product $product, ProductImageRepository $productImageRepository)
    {
        $shop = $product->getShop();
        $originalUsages = new ArrayCollection();
        foreach ($product->getUsages() as $usage) {
            $originalUsages->add($usage);
        }
        $originalLabels = new ArrayCollection();
        foreach ($product->getLabels() as $label) {
            $originalLabels->add($label);
        }

        $form = $this->createForm(FormProductType::class, $product)
            ->add('saveDashboard', SubmitType::class, ['label' => 'Enregistrer et retourner au tableau de bord'])
            ->add('saveShop', SubmitType::class, ['label' => 'Enregistrer et retourner au magasin']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($product->getUsages() as $usage) {
                $usage->addProduct($product);
            }
            foreach ($product->getLabels() as $label) {
                $label->addProduct($product);
            }
            //--On supprime les données supprimés
            foreach ($originalUsages as $usage) {
                if ($product->getUsages()->contains($usage) == false) {
                    $usage->removeProduct($product);
                    //$this->getDoctrine()->getManager()->remove($usage);
                }
            }
            foreach ($originalLabels as $label) {
                if ($product->getLabels()->contains($label) == false) {
                    $label->removeProduct($product);
                    //$this->getDoctrine()->getManager()->remove($label);
                }
            }
            $product->setUpdatedAt(new \DateTime());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

            if ($form->get('saveDashboard')->isClicked()) {
                return $this->redirectToRoute('collector_home');
            } else if ($form->get('saveShop')->isClicked()) {
                return $this->redirectToRoute('shop_show', ['slug' => $shop->getSlug()]);
            }
        }

        $selectedCategory = $product->getType();
        $productImages = $productImageRepository->findAll();
        $productImages = SerializerBuilder::create()->build()->serialize($productImages, 'json', SerializationContext::create()->setGroups(array('product_edit')));
        $form = $form->createView();
        return compact('product', 'shop', 'form', 'productImages', 'selectedCategory');
    }

    /**
     * @Route("/{id}/suppimer-produit", name="delete_product", methods={"DELETE"})
     */
    public function delete(Request $request, Product $product)
    {
        if ($this->isCsrfTokenValid('delete'.$product->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($product);
            $entityManager->flush();
        }

        return $this->redirectToRoute('collector_home');
    }
}
