<?php

namespace App\Command;

use App\Entity\ProductImage;
use App\Entity\ProductType;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\File;

class TaskCommand extends Command
{
    private $em;
    private $productImagePath;
    private $liipImagine;
    private $publicDir;

    public function __construct(
        EntityManagerInterface $em,
        $publicDir,
        $liipImagine,
        $productImagePath
    ) {
        $this->em = $em;
        $this->productImagePath = $productImagePath;
        $this->liipImagine = $liipImagine;
        $this->publicDir = $publicDir;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('task:command')
            ->setDescription('Cron to send automatic email')
            ->addArgument(
                'type',
                InputArgument::REQUIRED,
                'What kind of task do you want to execute ?'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $type = $input->getArgument('type');
        switch ($type) {
            //--exécution 7h
            case 'refresh_product_images':
                # send email last days promocode subscriptors
                $this->refreshProductImages($output);
                break;

            default:
                # error
                $output->writeln(sprintf('<error>[%s] Le type "%s" n\'est pas autorisé. La liste des types d\'emails est : inactive, middle-step ou end-step</error>', date('Y-m-d H:i:s'), $type));
                return 0;
        }

        /*$spool = $this->mailer->getTransport()->getSpool();
        $spool->flushQueue($this->transport);*/
        

        return 1;
    }
    private function refreshProductImages(OutputInterface $output) {
        $output->writeln(sprintf('<info>[%s] refresh product images.</info>', date('Y-m-d H:i:s')));

        //--On vidange les répertoires avant le script
        $path = $this->publicDir.$this->productImagePath;
        $derep=opendir($path);
        while($file = readdir($derep)){
            if($file != '..' && $file !='.' && $file !='' && $file!='.htaccess'){
                unlink($path.$file);
            }
        }
        $path = $this->publicDir."media/cache/product_thumb/images/products/";
        $derep=opendir($path);
        while($file = readdir($derep)){
            if($file != '..' && $file !='.' && $file !='' && $file!='.htaccess'){
                unlink($path.$file);
            }
        }

        //--On va récupérer les catégories
        $categories = $this->em->getRepository(ProductType::class)->findAll();

        //--On les parcours
        foreach($categories as $cat) {
            //--On va chercher le dossier correspondant à la catégorie
            $dir = realpath($this->publicDir . '/fixtures_imports/images_produits/'.$cat->getName());
            if ($dir !== false) {
                $files = scandir($dir);
                //--On va parcourir les fichiers du dossier
                foreach ($files as $file) {
                    if ($file !== '.' && $file !== '..') {
                        //--On créé l'objet
                        $productImage = new ProductImage();
                        $name = explode('.', $file)[0];
                        $productImage->setName($name);
                        $productImage->setProductType($cat);
                        if (!copy($dir.'/'.$file, $this->publicDir.$this->productImagePath.$file)) {
                            echo "La copie $file du fichier a échoué...\n";
                        } else {
                            $symfonyFile = new File($this->publicDir.$this->productImagePath.$file);
                            $productImage->setImageFile($symfonyFile);
                            $finalName = $productImage->getFileNamer();
                            rename($this->publicDir.$this->productImagePath.$file, $this->publicDir.$this->productImagePath.$finalName.".".$symfonyFile->getExtension());
                            $productImage->setImageName($finalName.".".$symfonyFile->getExtension());
                            $this->em->persist($productImage);
                            $this->em->flush();
                            $image = $this->liipImagine->getUrlOfFilteredImage($this->productImagePath.$productImage->getImageName(), 'product_thumb');
                        }
                    }
                }
            }
        }

        $output->writeln(sprintf('<info>[%s] end refresh product images.</info>', date('Y-m-d H:i:s')));
    }
}
