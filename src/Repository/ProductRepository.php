<?php

namespace App\Repository;

use Doctrine\ORM\Query\Expr;
use App\Entity\Product;
use App\Entity\Shop;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function search(String $search, 
                           ?array $types, 
                           ?array $usages, 
                           ?array $labels, 
                           ?String $origin, 
                           int $startLimit, 
                           float $resultsNumber, 
                           float $fromLng = null, 
                           float $fromLat = null, 
                           float $minLat = null, 
                           float $minLng = null, 
                           float $maxLat = null, 
                           float $maxLng = null)
    {
        $qb = $this->getSearchQueryBuilder($search,
                                            $types,
                                            $usages,
                                            $labels,
                                            $origin,
                                            $fromLng,
                                            $fromLat,
                                            $minLat, 
                                            $minLng, 
                                            $maxLat, 
                                            $maxLng);
        $qbCount = $this->getSearchQueryBuilder($search,
                                            $types,
                                            $usages,
                                            $labels,
                                            $origin,
                                            $fromLng,
                                            $fromLat,
                                            $minLat, 
                                            $minLng, 
                                            $maxLat, 
                                            $maxLng);
        $totalNumber = $qbCount->select('COUNT(p)')
            ->getQuery()
            ->getSingleScalarResult();
        //dump($qb->getQuery()->getSQL());
        //--On détermine si l'on a un point de départ et/ou un bornage
        $withFrom = false;
        if ($fromLat !== null && $fromLng !== null) {
            $withFrom = true;
        }
        //--On change l'ordre si un point de départ est définit
        if ($withFrom) {
            $qb->addOrderBy('distance', 'ASC');
            $qb->addSelect('ST_Distance(ST_MakePoint(:fromLng, :fromLat), a1.location) as distance')
                ->setParameter('fromLng', $fromLng)
                ->setParameter('fromLat', $fromLat);
            $qb->addOrderBy('p.name', 'ASC');
        } else {
            $qb->addOrderBy('p.origin', 'ASC');
            $qb->addOrderBy('p.name', 'ASC');
        }

        $results = $qb->setMaxResults($resultsNumber)
            ->setFirstResult($startLimit)
            ->distinct()
            ->getQuery()
            ->getResult();

        //--Si on a ajouté la distance dans les résultats, il faut les retraiter légèrement
        if ($withFrom) {
            $resulstWithDistance = [];
            foreach ($results as $result) {
                $theResult = $result[0];
                $theResult->setDistance(round($result['distance']));
                $resulstWithDistance[] = $theResult;
            }
            $results = $resulstWithDistance;
        }
        return [$results, $totalNumber];
    }

    public function getSearchQueryBuilder(String $search, 
                                            ?array $types, 
                                            ?array $usages, 
                                            ?array $labels, 
                                            ?String $origin, 
                                            float $fromLng = null, 
                                            float $fromLat = null, 
                                            float $minLat = null, 
                                            float $minLng = null, 
                                            float $maxLat = null, 
                                            float $maxLng = null)
    {
        $withFrom = $withBornage = false;
        if ($fromLat !== null && $fromLng !== null) {
            $withFrom = true;
        }
        if ($minLat !== null && $minLng !== null && $maxLat !== null && $maxLng !== null) {
            $withBornage = true;
        }
        $qb = $this->createQueryBuilder('p');
        
        $qb->andWhere( 'p.isPublished = :published')
            ->setParameter('published', true);
        $qb->join('p.shop', 's0')
            ->andWhere( 's0.isPublished = :published')
            ->setParameter('published', true);

        // Add word search in query builder
        if (!empty($search)) {
            
            $terms = explode(' ', $search);
            foreach ($terms as $i => $term) {
                //Dupliquer par termes
                $qb->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->like( 'LOWER(UNACCENT(p.name))', 'LOWER(UNACCENT(:term'.$i.'))'),
                        $qb->expr()->like( 'LOWER(UNACCENT(p.description))', 'LOWER(UNACCENT(:term'.$i.'))'),
                        $qb->expr()->like( 'LOWER(UNACCENT(p.producer))', 'LOWER(UNACCENT(:term'.$i.'))')
                    )
                )->setParameter('term'.$i, '%' . $term . '%');
            }
        }

        if ($types) {
            $qb->andWhere( 'p.type IN (:types)')
               ->setParameter('types', $types);
        }

        if ($usages) {
            $qb->join('p.usages', 'u');
            $qb->andWhere('u IN (:usages)')
               ->setParameter('usages', $usages);
        }

        if ($labels) {
            $qb->join('p.labels', 'l');
            $qb->andWhere( 'l IN (:labels)')
               ->setParameter('labels', $labels);
        }

        if ($origin) {
            switch ($origin) {
                case Product::ORIGIN_EUROPE:
                    $origins = [Product::ORIGIN_EUROPE, Product::ORIGIN_FRANCE, Product::ORIGIN_REGIONAL];
                    break;
                case Product::ORIGIN_FRANCE:
                    $origins = [Product::ORIGIN_FRANCE, Product::ORIGIN_REGIONAL];
                    break;
                case Product::ORIGIN_REGIONAL:
                    $origins = [Product::ORIGIN_REGIONAL];
                    break;
                default:
                    $origins = [Product::ORIGIN_WORLD, Product::ORIGIN_EUROPE, Product::ORIGIN_FRANCE, Product::ORIGIN_REGIONAL];
                    break;
            }
            $qb->andWhere( 'p.origin IN (:origins)')
               ->setParameter('origins', $origins);
        }

        //--SI on demande un point de départ géographique, on filtre sur un rayon de 8km
        if ($withFrom) {
            $qb->join('p.shop', 's1')
                ->join('s1.address', 'a1')
            ;
            if (!$withBornage) {
                $qb->andWhere(
                    $qb->expr()->eq(
                        "ST_DWithin(ST_MakePoint(:fromLng, :fromLat), a1.location, 8000)", 'true'
                    )
                )
                ->setParameter('fromLng', $fromLng)
                ->setParameter('fromLat', $fromLat);
            }

        }
        //--SI on demande un bornage géographique
        if ($withBornage) {
            $qb->join('p.shop', 's2')
                ->join('s2.address', 'a2')
                ->andWhere(
                    $qb->expr()->eq(
                        "ST_Intersects(ST_MakeEnvelope(:minX, :minY, :maxX, :maxY), a2.location)",
                        'true'
                    )
                )
                ->setParameter('minX', $minLng)
                ->setParameter('minY', $minLat)
                ->setParameter('maxX', $maxLng)
                ->setParameter('maxY', $maxLat)
            ;
        }
        return $qb;
    }

    public function searchInShop(Shop $shop,
                                String $search = '', 
                                ?array $types = null, 
                                ?array $usages = null, 
                                ?array $labels = null, 
                                ?String $origin = null) 
    {
        $qb = $this->createQueryBuilder('p');
        
        $qb->andWhere( 'p.isPublished = :published')
            ->setParameter('published', true);
        $qb->join('p.shop', 's0')
            ->andWhere( 's0 = :shop')
            ->setParameter('shop', $shop);

        // Add word search in query builder
        if (!empty($search)) {
            $terms = explode(' ', $search);
            foreach ($terms as $i => $term) {
                //Dupliquer par termes
                $qb->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->like( 'LOWER(UNACCENT(p.name))', 'LOWER(UNACCENT(:term'.$i.'))'),
                        $qb->expr()->like( 'LOWER(UNACCENT(p.description))', 'LOWER(UNACCENT(:term'.$i.'))'),
                        $qb->expr()->like( 'LOWER(UNACCENT(p.producer))', 'LOWER(UNACCENT(:term'.$i.'))')
                    )
                )->setParameter('term'.$i, '%' . $term . '%');
            }
        }

        if ($types) {
            $qb->andWhere( 'p.type IN (:types)')
               ->setParameter('types', $types);
        }

        if ($usages) {
            $qb->join('p.usages', 'u');
            $qb->andWhere('u IN (:usages)')
               ->setParameter('usages', $usages);
        }

        if ($labels) {
            $qb->join('p.labels', 'l');
            $qb->andWhere( 'l IN (:labels)')
               ->setParameter('labels', $labels);
        }

        if ($origin) {
            switch ($origin) {
                case Product::ORIGIN_EUROPE:
                    $origins = [Product::ORIGIN_EUROPE, Product::ORIGIN_FRANCE, Product::ORIGIN_REGIONAL];
                    break;
                case Product::ORIGIN_FRANCE:
                    $origins = [Product::ORIGIN_FRANCE, Product::ORIGIN_REGIONAL];
                    break;
                case Product::ORIGIN_REGIONAL:
                    $origins = [Product::ORIGIN_REGIONAL];
                    break;
                default:
                    $origins = [Product::ORIGIN_WORLD, Product::ORIGIN_EUROPE, Product::ORIGIN_FRANCE, Product::ORIGIN_REGIONAL];
                    break;
            }
            $qb->andWhere( 'p.origin IN (:origins)')
               ->setParameter('origins', $origins);
        }
        $qb->addOrderBy('p.name', 'ASC');

        $results = $qb->distinct()
            ->getQuery()
            ->getResult();

        return $results;
    }

    public function getMoreSignaledProductsInShop(Shop $shop) {
        $qb = $this->createQueryBuilder('p');
        $qb->select('p', 'count(sg)')
            ->join('p.shop', 's')
            ->leftJoin('p.signals', 'sg', Expr\Join::WITH, 'sg.status = \'open\'')
            ->andWhere( 's = :shop')
            ->andWhere( 'p.isPublished = :published')
            ->addOrderby('COUNT(sg.id)', 'DESC')
            ->addOrderby('p.updatedAt', 'DESC')
            ->groupBy('p')
            ->setMaxResults(50)
            ->setParameter('published', true)
            ->setParameter('shop', $shop);
        $results = $qb->getQuery()->getResult();
        return $results;
    }
}
