<?php

namespace App\Repository;

use App\Entity\ProductSignal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductSignal|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductSignal|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductSignal[]    findAll()
 * @method ProductSignal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductSignalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductSignal::class);
    }

    // /**
    //  * @return ProductSignal[] Returns an array of ProductSignal objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductSignal
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
