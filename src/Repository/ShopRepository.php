<?php

namespace App\Repository;

use App\Entity\Shop;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Shop|null find($id, $lockMode = null, $lockVersion = null)
 * @method Shop|null findOneBy(array $criteria, array $orderBy = null)
 * @method Shop[]    findAll()
 * @method Shop[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Shop::class);
    }

    public function search(String $search,
                           int $startLimit,
                           float $resultsNumber,
                           float $fromLng = null,
                           float $fromLat = null,
                           float $minLat = null,
                           float $minLng = null,
                           float $maxLat = null,
                           float $maxLng = null)
    {

        $qb = $this->getSearchQueryBuilder($search,
                                            $fromLng,
                                            $fromLat,
                                            $minLat,
                                            $minLng,
                                            $maxLat,
                                            $maxLng);
        $qbCount = $this->getSearchQueryBuilder($search,
                                            $fromLng,
                                            $fromLat,
                                            $minLat,
                                            $minLng,
                                            $maxLat,
                                            $maxLng);
        $totalNumber = $qbCount->select('COUNT(s)')
            ->getQuery()
            ->getSingleScalarResult();
        //--On détermine si l'on a un point de départ et/ou un bornage
        $withFrom = false;
        if ($fromLat !== null && $fromLng !== null) {
            $withFrom = true;
        }
        //--On change l'ordre si un point de départ est définit
        if ($withFrom) {
            $qb->addOrderBy('distance', 'ASC');
            $qb->addSelect('ST_Distance(ST_MakePoint(:fromLng, :fromLat), a1.location) as distance')
                ->setParameter('fromLng', $fromLng)
                ->setParameter('fromLat', $fromLat);
        } else {
            $qb->addOrderBy('s.name', 'ASC');
        }

        if ($withFrom) {
            $qb->addOrderBy('distance', 'ASC');
        } else {
            $qb->addOrderBy('s.name', 'ASC');
        }
        $results = $qb->setMaxResults($resultsNumber)
            ->setFirstResult($startLimit)
            ->distinct()
            ->getQuery()
            ->getResult();

        //--Si on a ajouté la distance dans les résultats, il faut les retraiter légèrement
        if ($withFrom) {
            $resulstWithDistance = [];
            foreach ($results as $result) {
                $theResult = $result[0];
                $theResult->setDistance(round($result['distance']));
                $resulstWithDistance[] = $theResult;
            }
            $results = $resulstWithDistance;
        }
        return [$results, $totalNumber];

    }

    private function getSearchQueryBuilder(String $search,
                                            float $fromLng = null,
                                            float $fromLat = null,
                                            float $minLat = null,
                                            float $minLng = null,
                                            float $maxLat = null,
                                            float $maxLng = null) {

        $withFrom = $withBornage = false;
        if ($fromLat !== null && $fromLng !== null) {
            $withFrom = true;
        }
        if ($minLat !== null && $minLng !== null && $maxLat !== null && $maxLng !== null) {
            $withBornage = true;
        }

        $qb = $this->createQueryBuilder('s');
        $qb->andWhere( 's.isPublished = :published')
            ->setParameter('published', true);

        // Add word search in query builder
        if (!empty($search)) {

            $qb->andWhere($qb->expr()->orX(
                $qb->expr()->like( 'LOWER(UNACCENT(s.name))', 'LOWER(UNACCENT(:search))'),
                $qb->expr()->like( 'LOWER(UNACCENT(s.description))', 'LOWER(UNACCENT(:search))')
            ))->setParameter('search', '%' . strtoupper($search) . '%');
        }

        //--SI on demande un point de départ géographique, on filtre sur un rayon de 8km
        if ($withFrom) {
            $qb->join('s.address', 'a1')
            ;
            if (!$withBornage) {
                $qb->andWhere(
                    $qb->expr()->eq(
                        "ST_DWithin(ST_MakePoint(:fromLng, :fromLat), a1.location, 8000)", 'true'
                    )
                )
                ->setParameter('fromLng', $fromLng)
                ->setParameter('fromLat', $fromLat);
            }

        }
        //--SI on demande un bornage géographique
        if ($withBornage) {
            $qb->join('s.address', 'a2')
                ->andWhere(
                    $qb->expr()->eq(
                        "ST_Intersects(ST_MakeEnvelope(:minX, :minY, :maxX, :maxY), a2.location)",
                        'true'
                    )
                )
                ->setParameter('minX', $minLng)
                ->setParameter('minY', $minLat)
                ->setParameter('maxX', $maxLng)
                ->setParameter('maxY', $maxLat)
            ;
        }
        return $qb;
    }
    /*public function findWithinArea(float $minLng, float $minLat, float $maxLng, float $maxLat)
    {
        $qb = $this->createQueryBuilder('s');
        $qb->andWhere(
            $qb->expr()->eq(
                "ST_Intersects(ST_MakeEnvelope(:minX, :minY, :maxX, :maxY), a.location)",
                'true'
            )
        )
        ->join('s.address', 'a')
        ->addSelect('a')
        ->setParameter('minX', $minLng)
        ->setParameter('minY', $minLat)
        ->setParameter('maxX', $maxLng)
        ->setParameter('maxY', $maxLat)
        ;
        return $qb->getQuery()
            ->getResult();
    }*/

    /**
     * @return Shop[] Returns an array of Shop objects
     */
    public function findAllActive()
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.isPublished = :val')
            ->setParameter('val', true)
            ->orderBy('s.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?Shop
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
