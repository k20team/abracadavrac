<?php

namespace App\Repository;

use App\Entity\OpeningTimes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OpeningTimes|null find($id, $lockMode = null, $lockVersion = null)
 * @method OpeningTimes|null findOneBy(array $criteria, array $orderBy = null)
 * @method OpeningTimes[]    findAll()
 * @method OpeningTimes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OpeningTimesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OpeningTimes::class);
    }

    // /**
    //  * @return OpeningTimes[] Returns an array of OpeningTimes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OpeningTimes
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
