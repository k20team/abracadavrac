<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use App\Form\Type\LocationType;

final class AddressAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('address', TextType::class, [
                'label' => 'Adresse',
                'attr' => ["class" => "address"]
            ])
            ->add('location', LocationType::class, [
                'label' => 'Location',
                'mapped' => false
            ])
            ->add('lat', TextType::class, [
                'label' => 'Latitude',
                'help' => 'Consulter http://geok2.k20web.fr/geocoder/geok2-client-simple-addresses.php',
                'attr' => ["class" => "lat"]
            ])
            ->add('lng', TextType::class, [
                'label' => 'Longitude',
                'help' => 'Consulter http://geok2.k20web.fr/geocoder/geok2-client-simple-addresses.php',
                'attr' => ["class" => "lng"]
            ])
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
    }
}
