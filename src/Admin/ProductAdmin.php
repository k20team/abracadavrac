<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Label;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Entity\Product;
use App\Entity\MeasureUnit;
use App\Entity\ProductImage;
use App\Entity\ProductSignal;
use App\Entity\ProductType;
use App\Entity\ProductUsage;
use App\Entity\Shop;
use App\Form\Type\ImageSelectorType;
use Sonata\AdminBundle\Form\Type\Filter\NumberType;
use Sonata\AdminBundle\Form\Type\Operator\NumberOperatorType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

final class ProductAdmin extends AbstractAdmin
{
    /*private $liipImagine;
    private $productImagePath;

    public function __construct($code, $class, $baseControllerName, $liipImagine, $productImagePath)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->liipImagine = $liipImagine;
        $this->productImagePath = $productImagePath;
    }*/

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('shop', null, [
                'label' => 'Magasin'
            ])
            ->add('name', null, [
                'label' => 'Nom'
            ])
            ->add('price', null, [
                'label' => 'Prix (€)'
            ])
            ->add('unit', null, [
                'label' => 'Unité'
            ])
            ->add('updatedAt', 'doctrine_orm_date_range', [
                'label' => 'Dernière MAJ'
            ])
            ->add('origin', null, [
                'label' => 'Origin'
            ])
            ->add('labels', null, [
                'label' => 'Label'
            ])
            ->add('producer', null, [
                'label' => 'Producteur'
            ])
            ->add('isPublished', null, [
                'label' => 'Publié'
            ])
            ->add('nbSignals', 'doctrine_orm_callback', array(
                'label' => 'Nombre de signalements',
                'callback' => function($queryBuilder, $alias, $field, $value) {
                    if (!$value || !isset($value['value']) || !isset($value['value']['type']) || !$value['value']['type']) {
                        return;
                    }

                    $operators = array(
                        NumberOperatorType::TYPE_EQUAL            => '=',
                        NumberOperatorType::TYPE_GREATER_EQUAL    => '>=',
                        NumberOperatorType::TYPE_GREATER_THAN     => '>',
                        NumberOperatorType::TYPE_LESS_EQUAL       => '<=',
                        NumberOperatorType::TYPE_LESS_THAN        => '<',
                    );

                    $operator = $operators[$value['value']['type']];

                    $queryBuilder->andWhere('SIZE('.$alias.'.signals) '.$operator.' :size');
                    $queryBuilder->setParameter('size', $value['value']['value']);
                },
                'field_type' => NumberType::class,
            ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('name', null, [
                'label' => 'Nom'
            ])
            ->add('shop', null, [
                'label' => 'Magasin'
            ])
            ->add('origin', null, [
                'label' => 'Origine'
            ])
            ->add('producer', null, [
                'label' => 'Producteur'
            ])
            ->add('priceToString', null, [
                'label' => 'Prix'
            ])
            ->add('isPublished', null, [
                'label' => 'Publié'
            ])
            ->add('signalsPriceNbr', null, [
                'label' => 'Nbr signalement prix'
            ])
            ->add('signalsStockNbr', null, [
                'label' => 'Nbr signalement stock'
            ])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('shop', EntityType::class, [
                'label' => 'Magasin',
                'class' => Shop::class,
                'choice_label' => 'name',
            ])
            ->add('type', EntityType::class, [
                'label' => 'Type',
                'class' => ProductType::class,
                'choice_label' => 'name',
                'attr' => [
                    //'data-sonata-select2' => 'false'
                    'class' => 'select-type'
                ]
            ])
            ->add('name', TextType::class, [
                'label' => 'Nom'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description',
                'required' => false
            ])
            ->add('productImage', ImageSelectorType::class, [
                'label' => 'Image',
                'required' => false,
                'class' => ProductImage::class,
                'choice_label' => 'name'
            ])
            ->add('price', TextType::class, [
                'label' => 'Prix',
                'help' => 'Indiquez les décimale avec un point.',
            ])
            ->add('unit', EntityType::class, [
                'label' => 'Unité',
                'class' => MeasureUnit::class,
                'choice_label' => 'shortName',
            ])
            ->add('origin', ChoiceType::class, [
                'required' => true,
                'label' => 'Origine',
                'choices' => [
                    'Region' => Product::ORIGIN_REGIONAL,
                    'France' => Product::ORIGIN_FRANCE,
                    'Europe' => Product::ORIGIN_EUROPE,
                    'Monde' => Product::ORIGIN_WORLD,
                ],
            ])
            ->add('labels', EntityType::class, [
                'label' => 'Labels',
                'class' => Label::class,
                'choice_label' => 'name',
                'multiple' => true
            ])
            ->add('usages', EntityType::class, [
                'label' => 'Usages',
                'class' => ProductUsage::class,
                'choice_label' => 'name',
                'multiple' => true
            ])
            ->add('producer', TextType::class, [
                'label' => 'Producteur / marque',
                'required' => false
            ])
            ->add('isPublished', CheckboxType::class, [
                'label' => 'Publié',
                'required' => false,
                'help' => "Si cette case n'est pas coché, le produit ne sera pas visible",
            ])
            ->add('razSignalPrice', CheckboxType::class, [
                'label' => 'RAZ des signalements de prix',
                'required' => false,
                'mapped' => false,
                'help' => "Cochez cette cas pour traiter tous les signalements de prix",
            ])
            ->add('razSignalStock', CheckboxType::class, [
                'label' => 'RAZ des signalements de stock',
                'required' => false,
                'mapped' => false,
                'help' => "Cochez cette cas pour traiter tous les signalements de stock",
            ])
            /*->add('isSignalPrice', CheckboxType::class, [
                'label' => 'Prix signalé',
                'required' => false,
                'help' => "Décocher cette case lorsque le signalement de prix est réglé",
            ])
            ->add('isSignalStock', CheckboxType::class, [
                'label' => 'Stock signalé',
                'required' => false,
                'help' => "Décocher cette case lorsque le signalement de stock est réglé",
            ])*/
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        /*$showMapper
            ->add('id')
            ->add('name')
            ->add('description')
            ->add('origin')
            ->add('producer')
            ->add('price')
            ->add('unit')
            ->add('priceUpdatedAt')
            ;*/
    }
    
    public function postPersist($product)
    {
        $this->signalManagement($product);
    }
    public function postUpdate($product)
    {
        $this->signalManagement($product);
    }
    private function signalManagement($product) {
        $product->setUpdatedAt(new \DateTime());
        $razSignalPrice = $this->getForm()->get('razSignalPrice')->getData();
        $razSignalStock = $this->getForm()->get('razSignalStock')->getData();
        $em = $this->getModelManager()->getEntityManager($this->getClass());
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        foreach ($product->getSignals() as $signal) {
            if ($razSignalPrice && $signal->getStatus() == ProductSignal::STATUS_OPEN && $signal->getType() == ProductSignal::TYPE_PRICE) {
                $signal->setStatus(ProductSignal::STATUS_CLOSED);
                $signal->setCorrectedAt(new \DateTime());
                $signal->setCorrectedBy($user);
                $em->persist($signal);
                $em->flush($signal);
            }
            if ($razSignalStock && $signal->getStatus() == ProductSignal::STATUS_OPEN && $signal->getType() == ProductSignal::TYPE_STOCK) {
                $signal->setStatus(ProductSignal::STATUS_CLOSED);
                $signal->setCorrectedAt(new \DateTime());
                $signal->setCorrectedBy($user);
                $em->persist($signal);
                $em->flush($signal);
            }
        }
    }
}
