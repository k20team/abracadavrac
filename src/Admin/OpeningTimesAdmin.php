<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;

final class OpeningTimesAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        /*$datagridMapper
            ->add('id')
            ->add('day')
            ->add('morningStartAt')
            ->add('morningEndAt')
            ->add('afternoonStartAt')
            ->add('afternoonEndAt')
            ;*/
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        /*$listMapper
            ->add('id')
            ->add('day')
            ->add('morningStartAt')
            ->add('morningEndAt')
            ->add('afternoonStartAt')
            ->add('afternoonEndAt')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);*/
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('day', TextType::class, [
                'label' => 'Jour',
                'attr' => array(
                    'readonly' => true,
                ),
            ])
            ->add('morningStartAt', TimeType::class, [
                'label' => 'Ouverture',
            ])
            ->add('morningEndAt', TimeType::class, [
                'label' => 'Fermeture (mi-journée)',
            ])
            ->add('afternoonStartAt', TimeType::class, [
                'label' => 'Ouverture (mi-journée)',
            ])
            ->add('afternoonEndAt', TimeType::class, [
                'label' => 'Fermeture',
            ])
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        /*$showMapper
            ->add('id')
            ->add('day')
            ->add('morningStartAt')
            ->add('morningEndAt')
            ->add('afternoonStartAt')
            ->add('afternoonEndAt')
            ;*/
    }
}
