<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class UserAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('email', null, [
                'label' => 'Email'
            ])
            ->add('firstname', null, [
                'label' => 'Prénom'
            ])
            ->add('lastname', null, [
                'label' => 'Nom'
            ])
            ->add('isEnabled', null, [
                'label' => 'Est actif'
            ])
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('email', null, [
                'label' => 'Email'
            ])
            ->add('firstname', null, [
                'label' => 'Prénom'
            ])
            ->add('lastname', null, [
                'label' => 'Nom'
            ])
            ->add('roles', null, [
                'label' => 'Role'
            ])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'impersonating' => ['template' => 'admin/impersonating.html.twig'],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('email', TextType::class, [
                'label' => 'Email'
            ])
            ->add('firstname', TextType::class, [
                'label' => 'Prénom'
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Nom'
            ])
            ->add('isEnabled', CheckboxType::class, [
                'label' => 'Est actif',
                'required' => false
            ])
            ->add('isTest', CheckboxType::class, [
                'label' => 'Est un utilisateur test',
                'required' => false
            ])
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    'Collecteur' => 'ROLE_COLLECTOR',
                    'Administrateur' => 'ROLE_ADMIN',
                ],
                'expanded'  => false, // liste déroulante
                'multiple'  => true, // choix multiple
                'required' => false
            ])
            ->add('managedShops', EntityType::class, [
                'label'        => 'Magasins gérés (si collecteur)',
                'class'        => \App\Entity\Shop::class,
                'required'     => false,
                'multiple'     => true,
                'by_reference' => false,
                'help' => 'Le lien n\'a d\'intéret seulement si l\'utilisateur lié est collecteur',
            ])
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        /*$showMapper
            ->add('id')
            ->add('email')
            ->add('firstname')
            ->add('lastname')
            ->add('roles')
            ->add('password')
            ->add('isEnabled')
            ->add('isTest')
            ;*/
    }
    public function prePersist($user)
    {
        $this->generatePassword($user);
    }
    public function preUpdate($user)
    {
        //$this->save($shop);
    }

    private function generatePassword($user) {
        $user->setPassword("provisoire");
    }
}
