<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\ContactMessage;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Route\RouteCollection;

final class ContactMessageAdmin extends AbstractAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->add('treateMessage', $this->getRouterIdParameter().'/traiter-message');
        $collection->add('untreateMessage', $this->getRouterIdParameter().'/detraiter-message');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('name', null, [
                'label' => 'Nom'
            ])
            ->add('email', null, [
                'label' => 'Email'
            ])
            ->add('state', 'doctrine_orm_choice', [
                'field_type' => ChoiceType::class,
                'field_options' => [
                    'choices' => [
                        'En attente' => ContactMessage::STATUS_WAITING,
                        'Traité' => ContactMessage::STATUS_DONE,
                    ]
                ]
            ])
            ->add('sendedAt', null, [
                'label' => 'Envoyé le',
            ])
        ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('name', null, [
                'label' => 'Nom'
            ])
            ->add('email', null, [
                'label' => 'Email'
            ])
            ->add('text', null, [
                'label' => 'Texte'
            ])
            ->add('sendedAt', null, [
                'label' => 'Envoyé le',
            ])
            ->add('state', null, [
                'label' => 'Etat',
            ])
            ->add('_action', null, [
                'actions' => [
                    'treatment' => ['template' => 'admin/list__messageTreatment.html.twig'],
                ],
            ])
            ;
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        /*$formMapper
            ->add('name', TextType::class, [
                'label' => 'Nom'
            ])
            ->add('email', TextType::class, [
                'label' => 'Email',
                'required' => false
            ])
            ->add('text', TextareaType::class, [
                'label' => 'Texte',
                'required' => false
            ])
            ->add('state', ChoiceType::class, [
                'label' => 'Action',
                'required' => false,
                'choices' => [
                    'En attente' => ContactMessage::STATUS_WAITING,
                    'Traité' => ContactMessage::STATUS_DONE,
                ],
            ])
            ;*/
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
    }

}
