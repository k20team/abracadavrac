<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\ProductType;

final class ProductImageAdmin extends AbstractAdmin
{
    private $liipImagine;
    private $productImagePath;

    public function __construct($code, $class, $baseControllerName, $liipImagine, $productImagePath)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->liipImagine = $liipImagine;
        $this->productImagePath = $productImagePath;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('name', null, [
                'label' => 'Nom'
            ])
            ->add('productType', null, [
                'label' => 'Type de produit'
            ])
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('name', null, [
                'label' => 'Nom'
            ])
            ->add('productType', null, [
                'label' => 'Type de produit'
            ])
            ->add('nbrAssociatedProducts', null, [
                'label' => 'Nombre de produits associés'
            ])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('name', TextType::class, [
                'label' => 'Nom'
            ])
            ->add('productType', EntityType::class, [
                'label' => 'Type',
                'class' => ProductType::class,
                'choice_label' => 'name'
            ])
            ->add('imageFile', VichImageType::class, array(
                'attr' => ['showImage' => true, 'showThumb' => true],
                'label' => 'Image',
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false,
            ))
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        /*$showMapper
            ->add('id')
            ->add('name')
            ->add('imageName')
            ;*/
    }
    
    public function postPersist($productImage)
    {
        $this->generateImages($productImage);
    }
    public function postUpdate($productImage)
    {
        $this->generateImages($productImage);
    }
    private function generateImages($productImage) {
        if ($productImage->getImageName() != null && $productImage->getImageName() != '') {
            $image = $this->liipImagine->getUrlOfFilteredImage($this->productImagePath.'/'.$productImage->getImageName(), 'product_thumb');
        }
    }
}
