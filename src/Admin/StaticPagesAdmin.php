<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

final class StaticPagesAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('title')
            ->add('slug')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('title')
            ->add('slug')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('title')
            ->add('slug')
            ->add('content', CKEditorType::class, array(
                    'label' => 'Contenu',
                    'config' => array(
                        'toolbar' => 'full' /*array(
                            array(
                                'name'  => 'document',
                                'items' => array('Source', '-', 'Bold', 'Italic', 'Underline', '-', 'Link', 'Unlink', '-', 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'),
                            ),
                        ),*/
                    )
                )
            )
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        /*$showMapper
            ->add('title')
            ->add('slug')
            ->add('content')
            ;*/
    }

    /*public function prePersist($staticPages)
    {
        $this->generateSlug($staticPages);
    }
    public function preUpdate($staticPages)
    {
        $this->generateSlug($staticPages);
    }

    private function generateSlug($staticPages) {

        $shop->getAddress()->updateLocation();
    }*/
}
