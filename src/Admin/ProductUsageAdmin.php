<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Vich\UploaderBundle\Form\Type\VichImageType;

final class ProductUsageAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('name')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('name', null, [
                'label' => 'Nom'
            ])
            ->add('appearanceOrder', null, [
                'label' => "Ordre d'affichage"
            ])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('name', TextType::class, [
                'label' => 'Nom'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description',
                'required' => false
            ])
            ->add('appearanceOrder', IntegerType::class, [
                'label' => "Ordre d'affichage",
                'required' => false
            ])
            ->add('coverFile', VichImageType::class, array(
                'attr' => ['showImage' => true, 'showThumb' => false],
                'label' => 'Couverture',
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false,
            ))
            ->add('exagonImageFile', VichImageType::class, array(
                'attr' => ['showImage' => true, 'showThumb' => false],
                'label' => 'Image exagonale',
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false,
            ))
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        /*$showMapper
            ->add('name')
            ;*/
    }
}
