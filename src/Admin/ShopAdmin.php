<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Form\Type\AdminType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\ShopType;
use Sonata\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Vich\UploaderBundle\Form\Type\VichImageType;

final class ShopAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('name', null, [
                'label' => 'Nom'
            ])
            ->add('isPublished', null, [
                'label' => 'Publié'
            ])
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('name', null, [
                'label' => 'Nom'
            ])
            ->add('email', null, [
                'label' => 'Email'
            ])
            ->add('phone', null, [
                'label' => 'Téléphone'
            ])
            ->add('website', null, [
                'label' => 'Site internet'
            ])
            ->add('address', 'text', [
                'label' => 'Adresse'
            ])
            ->add('isPublished', null, [
                'label' => 'Publié'
            ])
            ->add('_action', null, [
                'actions' => [
                    'products' => ['template' => 'admin/shop-products.html.twig'],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('name', TextType::class, [
                'label' => 'Nom'
            ])
            ->add('logoFile', VichImageType::class, array(
                'attr' => ['showImage' => true, 'showThumb' => false],
                'label' => 'Logo',
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false,
            ))
            ->add('types', EntityType::class, [
                'label' => 'Types',
                'class' => ShopType::class,
                'choice_label' => 'name',
                'multiple' => true
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description',
                'required' => false
            ])
            ->add('openingTimes', CollectionType::class, [
                'label' => 'Horaires',
                'type_options' => [
                    // Prevents the "Delete" option from being displayed
                    'btn_delete' => false,
                    'btn_add' => false,
                ]
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'test' => false,
                'allow_delete' => false,
            ])
            ->add('email', TextType::class, [
                'label' => 'Email',
                'required' => false
            ])
            ->add('phone', TextType::class, [
                'label' => 'Téléphone',
                'required' => false
            ])
            ->add('website', UrlType::class, [
                'label' => 'Site internet',
                'required' => false
            ])
            //--Ici, il va chercher AddressAdmin automatiquement
            ->add('address', AdminType::class, [
                'label' => 'Adresse et localisation'
            ])

            ->add('managedBy', EntityType::class, [
                'label'        => 'Collecteurs associés',
                'class'        => \App\Entity\User::class,
                'required'     => false,
                'multiple'     => true,
                'by_reference' => false,
                'choices' => $this->getCollectors(),
            ])
            ->add('isPublished', CheckboxType::class, [
                'label' => 'Publié',
                'required' => false,
                'help' => "Si cette case n'est pas coché, le magasin ne sera pas visible",
            ])
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
    }


    public function prePersist($shop)
    {
        $this->save($shop);
    }
    public function preUpdate($shop)
    {
        $this->save($shop);
    }

    private function save($shop) {
        $shop->getAddress()->updateLocation();
    }

    private function getCollectors(){
        $entity = new \App\Entity\User();
        $query = $this->modelManager->getEntityManager($entity)->createQuery('SELECT s FROM App\Entity\User s ORDER BY s.id ASC');
        $collectors_brute = $query->getResult();
        $collectors = array();
        foreach ($collectors_brute as $collector) {
            foreach ($collector->getRoles() as $role) {
                if ($role == 'ROLE_COLLECTOR') {
                    $collectors[] = $collector;
                }
            }
        }
        return $collectors;
    }
}
