<?php

namespace App\Services;

/**
 * Address to location and location to address converter.
 * Only one request can be made.
 * Create another object to make another request.
 */
class GeoK2
{
    const API_URL = [
        'location-simple' => 'https://api-adresse.data.gouv.fr/search/',
        'address-simple' => 'https://api-adresse.data.gouv.fr/reverse/',
    ];

    private $parameters;
    /*
    private $coordinates;
    private $addresses;
    private $locations;*/

    /**
     * @param Array $options    Some options to configure the library
     * @param Array $parameters Url parameters to add when making a request
     * See https://adresse.data.gouv.fr/api#search
     */
    public function __construct($defaultParameters = [])
    {
        $this->parameters = $defaultParameters;

        $this->coordinates = null;
        $this->addresses = null;
        $this->locations = null;
    }

    public function getSimpleLocation($address): ?Array
    {
        $json = $this->makeSimpleRequest(self::API_URL['location-simple'], ['q' => $address]);
        $results = $this->parseJSON($json);
        foreach ($results as $result) {
            $result->setRequest($address);
        }
        return $results;
    }

    public function getBestSimpleLocation($address): ?GK2Location
    {
        /*$json = $this->getSimpleLocation($address);
        $results = $this->parseJSON($json);
        foreach ($results as $result) {
            $result->setRequest($address);
        }*/
        $results = $this->getSimpleLocation($address);
        //--On récupère le meilleur score
        return array_reduce(
            $results,
            function($best, $location) {
                return null === $best || $location->getScore() > $best->getScore()
                    ? $location : $best;
            }
        );
    }

    public function getSimpleAddress($coordinates): ?Array
    {
        [$lng, $lat] = $coordinates;
        $json = $this->makeSimpleRequest(self::API_URL['address-simple'], [
            'lon' => $lng,
            'lat' => $lat,
        ]);
        $results = $this->parseJSON($json);
        foreach ($results as $result) {
            $result->setRequest('Latitude : '.$result->getLat().', Longitude : '.$result->getLng());
        }
        return $results;
    }
    public function getBestSimpleAddress($coordinates): ?GK2Location
    {
        $results = $this->getSimpleAddress($coordinates);
        //--On récupère le meilleur score
        return array_reduce(
            $results,
            function($best, $location) {
                return null === $best || $location->getScore() > $best->getScore()
                    ? $location : $best;
            }
        );
    }

    public function parseJSON($json): Array
    {
        $data = json_decode($json);

        $features = $data->features;
        $locations = [];

        foreach ($features as $feature) {
            $props = $feature->properties;

            [$lng, $lat] = $feature->geometry->coordinates;
            $locations[] = (new GK2Location())
                    ->setLat((float) $lat)
                    ->setLng((float) $lng)
                    ->setAddress($props->label)
                    ->setScore((float) $props->score);
            ;
        }

        return $locations;
    }

    public function makeSimpleRequest(string $url, Array $parameters, Array $httpConfig = []): string
    {
        if (!isset($httpConfig['method'])) {
            $httpConfig['method'] = 'GET';
        }

        if (!empty($parameters)) {
            if ($httpConfig['method'] === 'GET') {
                $paramsStr = [];

                foreach ($parameters as $parameter => $value) {
                    $value = urlencode($value);
                    $paramsStr[] = "$parameter=$value";
                }

                $url .= '?' . join('&', $paramsStr);
            } else if ($httpConfig['method'] === 'POST' || $httpConfig['method'] === 'PUT') {
                $httpConfig['header'] = 'Content-type: application/x-www-form-urlencoded';
                $httpConfig['content'] = http_build_query($parameters);
            }
        }

        $context = stream_context_create([
            'http' => $httpConfig,
        ]);

        return file_get_contents($url, false, $context);
    }

}

class GK2Location
{
    /**
     * The request.
     *
     * @var string
     */
    private $request;

    /**
     * The address corresponding to the location.
     *
     * @var string
     */
    private $address;

    /**
     * The latitude of the location.
     *
     * @var float
     */
    private $lat;

    /**
     * The longitude of the location.
     *
     * @var float
     */
    private $lng;

    /**
     * The 'score' of the location.
     * This is the 'chance' that the coordinates correspond to the address.
     * Score in between 0 and 1.
     *
     * @var float
     */
    private $score;

     public function __construct($defaultParameters = [])
    {
        $this->address = '';
    }

    public function getRequest(): string
    {
        return $this->request;
    }

    public function setRequest(?string $request): self
    {
        $this->request = $request;

        return $this;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getLat(): float
    {
        return $this->lat;
    }

    public function setLat(?float $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLng(): float
    {
        return $this->lng;
    }

    public function setLng(?float $lng): self
    {
        $this->lng = $lng;

        return $this;
    }

    public function getScore(): float
    {
        return $this->score;
    }

    public function setScore(?float $score): self
    {
        if ($score < 0 || $score > 1) {
            throw new RuntimeException('Score must be greater than 0 and less than 1');
        }

        $this->score = $score;

        return $this;
    }

}
