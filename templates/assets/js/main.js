'use strict';

const $ = require('jquery');

// Include Jquery global application
global.$ = global.jQuery = $;

import 'bootstrap/js/dist/modal';
import 'bootstrap/js/dist/collapse';
import 'bootstrap/js/dist/dropdown';

if (jQuery('#vue-menu-burger').length > 0) {
    import('./vuejs/utilities/vue-menu-burger');
}
if ($('#home').length > 0) {
    import ('./vuejs/home');
}
if ($('#search-shop').length > 0) {
    import ('./vuejs/searchShop');
}
if ($('#search-product').length > 0) {
    import ('./vuejs/searchProduct');
}
if ($('#shop-page').length > 0) {
    import('./vuejs/shop');
}
if ($('#collector-home').length > 0) {
    import('./vuejs/collector');
}
if ($('#edit-product').length > 0) {
    import('./vuejs/editProduct');
}


$(".bio").hide();
$(".show_hide_bio").on("click", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var txt = $(".bio").is(':visible') ? 'Voir plus' : 'Voir moins';
    $(".show_hide_bio").text(txt);
    $(this).next('.bio').slideToggle(200);
    return false;
});

$(".protected-territory").hide();
$(".show_hide_protected_territory").on("click", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var txt = $(".protected-territory").is(':visible') ? 'Voir plus' : 'Voir moins';
    $(".show_hide_protected_territory").text(txt);
    $(this).next('.protected-territory').slideToggle(200);
    return false;
});

$(".without-gluten").hide();
$(".show_hide_without_gluten").on("click", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var txt = $(".without-gluten").is(':visible') ? 'Voir plus' : 'Voir moins';
    $(".show_hide_without_gluten").text(txt);
    $(this).next('.without-gluten').slideToggle(200);
    return false;
});

$(".equity-trade").hide();
$(".show_hide_equity_trade").on("click", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var txt = $(".equity-trade").is(':visible') ? 'Voir plus' : 'Voir moins';
    $(".show_hide_equity_trade").text(txt);
    $(this).next('.equity-trade').slideToggle(200);
    return false;
});


$(".blue-red-heart").hide();
$(".show_hide_blue_red_heart").on("click", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var txt = $(".blue-red-heart").is(':visible') ? 'Voir plus' : 'Voir moins';
    $(".show_hide_blue_red_heart").text(txt);
    $(this).next('.blue-red-heart').slideToggle(200);
    return false;
});

$(".vegan").hide();
$(".show_hide_vegan").on("click", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var txt = $(".vegan").is(':visible') ? 'Voir plus' : 'Voir moins';
    $(".show_hide_vegan").text(txt);
    $(this).next('.vegan').slideToggle(200);
    return false;
});


$(".slow-cosmetic").hide();
$(".show_hide_slow_cosmetic").on("click", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var txt = $(".slow-cosmetic").is(':visible') ? 'Voir plus' : 'Voir moins';
    $(".show_hide_slow_cosmetic").text(txt);
    $(this).next('.slow-cosmetic').slideToggle(200);
    return false;
});