import Vue from 'vue';
import addressControl from './components/addressControl';
import Slick from 'vue-slick';
import 'slick-carousel/slick/slick.css'

new Vue({
    delimiters: ['${', '}'],
    el: '#home',
    components: {
        addressControl,
        Slick
    },
    data() {
        return {
            slickOptions: {
                slidesToShow: 4,
                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 3,
                        }
                    },
                    {
                        breakpoint: 576,
                        settings: {
                            slidesToShow: 1,
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            },
        };
    },
    methods: {
        next() {
            this.$refs.slick.next();
        },

        prev() {
            this.$refs.slick.prev();
        },
        reInit() {
            // Helpful if you have to deal with v-for to update dynamic lists
            this.$nextTick(() => {
                this.$refs.slick.reSlick();
            });
        },
    }
});
