import Vue from 'vue';
import mixinOpeningTimes from './mixins/openingTimes';
import handleScroll from './mixins/handleScroll';
import axios from 'axios';

import {
    fromIcon,
    selectedIcon
} from "./utilities/markers-leaflet";


Vue.use(require('vue-moment'));
Vue.use(require('./utilities/vue-truncate'));

new Vue({
    delimiters: ['${', '}'],
    el: '#shop-page',
    mixins: [mixinOpeningTimes, handleScroll],
    data: {
        productShopList: datas.productShopList,
        openingTimes: datas.openingTimes,
        shopSlug: datas.shopSlug,
        //searchText: '',
        productShowing: '',
        filterShowing: '',
        orderedCategories: datas.orderedCategories,
        signalProductShowing: false,
        signalPrice: false,
        signalStock: false,
        signalDescription: '',
        signalSuccess: false,
        signalWarning: false,
        productImagesPath: datas.productImagesPath,
    },
    mounted() {
        //console.log(this.productShopList)
        //console.log(this.orderedCategories)
    },
    watch: {
        filterShowing: function (val) {
            (val === 'show' || val === 'open') ? document.body.classList.add('is-locked'): document.body.classList.remove('is-locked')
        },
        productShowing: function (val) {
            if (val != '') {
                document.body.classList.add('is-locked')
                this.signalSuccess = false
                this.signalWarning = false
            } else {
                document.body.classList.remove('is-locked')
                this.signalSuccess = false
                this.signalWarning = false
            }
        },
    },
    methods: {
        search: function() {
            var searchForm = $('#search-form');
            var formDatas = searchForm.serialize();
            var self = this
            $('#collapse-form').collapse('hide');
            this.filterShowing = '';
            $.get(
                Routing.generate("shop_show", { 'slug': this.shopSlug }),
                formDatas,
                function(datas) {
                    self.productShopList = datas;
                }
            );
        },
        imageProduct: function (product) {
            if (typeof product.product_image === 'undefined') {
                return "/dist/img/" + product.type.icon + "_default.svg"
            }
            return this.productImagesPath + "/" + product.product_image.image_name;
        },
        signalError: function() {
            var _self = this
            var product = this.productShowing;
            var price = 0
            var stock = 0
            if (this.signalPrice) {
                price = 1
            }
            if (this.signalStock) {
                stock = 1
            }
            var description = this.signalDescription;

            if(price == 0 && stock == 0) {
                this.signalWarning = true
            } else {
                axios.get(Routing.generate("signal_product", { 'product': product, 'price': price, 'stock': stock, 'description': description })).then(function (response) {
                    _self.signalProductShowing = false;
                    _self.signalDescription = ''
                    _self.signalStock = false
                    _self.signalPrice = false
                    _self.signalSuccess = true
                    _self.signalWarning = false
                })
                .catch(err => console.log(err))
            }
        },
        linkToEditProduct: function(product) {
            return Routing.generate('edit_product', { 'product': product.id })
        },
        getSignalByProduct: function(productIndex, indexCategorized, typeSignal) {
            var signal = this.productListCategorized[indexCategorized][productIndex].signals.filter((signal) => signal.status == 'open' && signal.type == typeSignal)
            if (signal.length > 0)
                return true
            else
                return false
        },
    },
    computed: {
        productListCategorized: function () {

            var productToFilter = this.productShopList

            // Recatégorisation des produits pour l'affichage
            var list = new Array

            for (var i = 0; i < this.orderedCategories.length; i++) {
                var typeId = this.orderedCategories[i].id
                var typeOrder = this.orderedCategories[i].appearance_order
                if (typeof list[typeOrder] === 'undefined') {
                    var products = productToFilter.filter(item => item.type.id == typeId);
                    if (products.length > 0) {
                        list[typeOrder] = products
                    }
                }
            }

            //--Nettoie les éléments vides du tableau et réattribue les index 0 , 1
            list = list.filter(Boolean)
            return list
        }

    }
});
