import Vue from 'vue';
import axios from 'axios';
import addressControl from './components/addressControl';
import mixinDetictingMobile from './mixins/isMobile';
import mixinOpeningTimes from './mixins/openingTimes';
import handleScroll from './mixins/handleScroll';

import {
    fromIcon,
    selectedIcon
} from "./utilities/markers-leaflet";

import Slick from 'vue-slick';
import 'slick-carousel/slick/slick.css'

Vue.use(require('vue-moment'));
Vue.use(require('./utilities/vue-truncate'));

var searchProduct = new Vue({
    delimiters: ['${', '}'],
    el: '#search-product',
    mixins: [mixinDetictingMobile, handleScroll, mixinOpeningTimes],
    components: {
        addressControl,
        Slick
    },
    data: {
        from: datas.from,
        center: datas.center,
        zoom: datas.zoom,
        bounds: null,
        savedBounds: null,
        showedNumber: datas.showedNumber,
        map: null,
        moved: false,
        searchInProgress: false,
        productList: datas.productList,
        shopList: null,
        selectedShop: null,
        markersList: null,
        startLimit: datas.startLimit,
        nbrResults: datas.productList.length,
        nbrResultsTotal: datas.totalNumber,
        currentPage: 1,
        timerMove: null,
        isRealyInit: false,
        fromMarker: null,
        mobileList: true,
        mobileMap: false,
        filterShowing: '',
        activeFilters: null,
        productShowing: '',
        errorSignal: '',
        form_what: datas.what,
        productImagesPath: datas.productImagesPath,
        signalProductShowing: false,
        signalPrice: false,
        signalStock: false,
        signalDescription: '',
        signalSuccess: false,
        signalWarning: false,

        slickOptions: {
            slidesToShow: 1,
            arrows: false,
            infinite: false,
            centerMode: true,
            centerPadding: '20px',
            mobileFirst: true,
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        centerPadding: '40px',
                    }
                }
            ]
        },
    },

    mounted() {
        //console.log(this.productList)
        this.map = this.$refs.map.mapObject
        this.map.scrollWheelZoom.disable();
        this.addFromIcon();
        this.bounds = this.map.getBounds();
        this.drawShops();
        if (this.productList.length > 0) {
            this.fitBoundsToShops();
        }

        this.activeFilters = new Array;
        this.updateActiveFilters();

        //--Initialisation (seulement en desktop)
        if (!this.isMobile()) {
            this.zoom = this.map.getZoom();
            this.center = this.map.getCenter();
            this.bounds = this.map.getBounds();
        }

        this.$refs.slick.$on('afterChange', this.handleAfterChange);
    },

    watch: {
        filterShowing: function (val) {
            (val === 'show' || val === 'open') ? document.body.classList.add('is-locked') : document.body.classList.remove('is-locked')
        },
        productShowing: function (val) {
            if (val != '') {
                document.body.classList.add('is-locked')
                this.signalSuccess = false
                this.signalWarning = false
            } else {
                document.body.classList.remove('is-locked')
                this.signalSuccess = false
                this.signalWarning = false
            }
        },
        mobileMap: function (val) {
            if (val) {
                this.$nextTick(() => {
                    this.map.invalidateSize(true)
                    this.map.setView([this.center.lat, this.center.lng], this.zoom);
                    this.$refs.slick.reSlick();
                    this.initMapFromSlider()
                });
            }
        },
        productList: function (val) {
            if (this.mobileMap) {
                this.$refs.slick.destroy()
                this.selectedShop = null
                this.$nextTick(() => {
                    this.$refs.slick.create()
                    this.initMapFromSlider()
                })
            }
        }
    },

    computed: {
        totalPageNumber: function() {
            return Math.ceil(this.nbrResultsTotal / this.showedNumber)
        }
    },

    methods: {
        syncFormWhat: function(elem) {
            this.form_what = elem.target.value
        },
        // Methode de recherche
        search: function () {
          var self = this;
          self.submit('new');
        },
        submit: function (type) {
            document.documentElement.scrollTop = 0
            this.searchInProgress = true;
            this.filterShowing = '';
            this.productShowing = '';
            if (!this.isMobile()) $('#collapse-form').collapse('hide');

            var self = this;
            if (type == 'new' || type == 'map') {
                self.startLimit = 0;
                self.currentPage = 1;
            } else if (type == 'next') {
                self.startLimit = self.startLimit + self.showedNumber;
                self.currentPage = self.currentPage + 1;
            } else if (type == 'prev') {
                self.startLimit = self.startLimit - self.showedNumber;
                self.currentPage = self.currentPage - 1;
            }
            var searchForm = $('#search-form');
            var formDatas = searchForm.serialize();

            //--On ajoute l'url à l'historique
            var url = Routing.generate("search_product")+"?"+formDatas;
            window.history.pushState('', '', url);

            //--On ajoute les données utiles
            formDatas = formDatas + "&startLimit=" + self.startLimit + "&ajax=1";
            //--Si la recherche vient de la map, on borne sur le scope de la map
            if ((type == 'map' || type == 'next' || type == 'prev') && self.savedBounds != null ) {
                if (!self.isMobile() || (self.isMobile() && self.mobileMap)) {
                    formDatas = formDatas + "&minLat=" + self.savedBounds._southWest.lat + "&minLng=" + self.savedBounds._southWest.lng + "&maxLat=" + self.savedBounds._northEast.lat + "&maxLng=" + self.savedBounds._northEast.lng;
                }
            }
            $.get(
                Routing.generate("search_product"),
                formDatas,
                function(datas) {
                    self.productList = datas[0];
                    self.drawShops();
                    //--On recadre sur les markers seulement si la recherche ne vient pas d'un movement de la carte
                    self.nbrResults = datas[0].length
                    self.nbrResultsTotal = datas[1]
                    self.searchInProgress = false;
                    var lng = datas[2];
                    var lat = datas[3];
                    var fromRequest = datas[4];

                    if (fromRequest && lng && lat) {
                        self.from = [parseFloat(lat), parseFloat(lng)]
                    } else {
                        self.from = null
                    }
                    self.addFromIcon()
                    if (type == 'new') {
                        if (self.productList.length > 0) {
                            self.fitBoundsToShops();
                        }
                        self.updateActiveFilters();
                    }
                }
            );
            /*axios.post(Routing.generate("search_product"), formDatas)
                .then(function (response) {
                    console.log(response)
                    var datas = response.data
                    self.productList = datas[0]
                    self.drawShops()
                    //--On recadre sur les markers seulement si la recherche ne vient pas d'un movement de la carte
                    self.nbrResults = datas[0].length
                    self.nbrResultsTotal = datas[1]
                    self.searchInProgress = false

                    if (datas[2] && datas[3]) {
                        self.from = [parseFloat(datas[3]), parseFloat(datas[2])]
                    } else {
                        self.from = null
                    }
                    self.addFromIcon()
                    if (type == 'new') {
                        self.fitBoundsToShops();
                    }
                })
                .catch(err => console.log(err))*/
        },
        drawShops: function() {
            var shops = new Array;
            if (this.markersList != null) {
                for (var i = 0; i < this.markersList.length; i++) {
                    this.map.removeLayer(this.markersList[i]);
                }
            }
            this.markersList = new Array;
            for (var i = 0; i < this.productList.length; i++) {
                shops.push(this.productList[i].shop);
            }
            //--Unique
            this.shopList = Array.from(new Set(shops.map(s => s.id))).map(id => {
                return shops.find(s => s.id === id)
            });
            //--On dessine les markers
            for (var i = 0; i < this.shopList.length; i++) {
               //--le marker sélectionné
                if (this.selectedShop != null && this.shopList[i].id == this.selectedShop.id) {
                    var marker = L.marker(L.latLng(this.shopList[i].address.lat, this.shopList[i].address.lng), {icon: selectedIcon})
                    .addTo(this.map)
                    .bindPopup(this.getPopup(this.shopList[i])).openPopup()
                    .on('click', this.clickOnShop.bind(null, this.shopList[i]));
                } else { //--Les autres markers
                    var marker = L.marker(L.latLng(this.shopList[i].address.lat, this.shopList[i].address.lng))
                    .addTo(this.map)
                    .on('click', this.clickOnShop.bind(null, this.shopList[i]));
                }
                this.markersList.push(marker);
            }
        },
        clickOnShop: function(shop, e) {
            this.selectedShop = shop;
            this.selectShop();
        },
        clickOnProduct: function(product) {
            this.productShowing = product.id;
            if (this.selectedShop != product.shop){
                this.selectedShop = product.shop;
                this.selectShop();
            }
            this.centerOnSelectedShop();
        },
        mouseOverOnProduct: function(shop) {
            if (this.selectedShop != shop){
                this.selectedShop = shop;
                this.selectShop();
            }
        },
        initMapFromSlider: function() {
            if (this.selectedShop === null) {
                var currentShop = this.$refs.slick.$el.slick.$slides[0].childNodes[0].childNodes[0].dataset.shop;
                this.selectShopFromSlider(currentShop)
            }
            this.selectShop();
            this.centerOnSelectedShop();
        },
        handleAfterChange: function (event, slick, currentSlide) {
            var currentShop = slick.$slides[currentSlide].childNodes[0].childNodes[0].dataset.shop;
            this.selectShopFromSlider(currentShop)
            this.selectShop();
            this.centerOnSelectedShop();
        },
        selectShopFromSlider: function(currentShopId) {
            var shop = null;
            for (var i = 0; i < this.shopList.length; i++) {
                if (this.shopList[i].id == currentShopId) {
                    shop = this.shopList[i]
                }
            }
            this.selectedShop = shop;
        },
        selectShop: function() {
            var allCards = document.querySelectorAll("[data-shop]");
            for (var i = 0; i < allCards.length; i++) {
                allCards[i].classList.remove("selected");
            }
            var cards = document.querySelectorAll("[data-shop='" + this.selectedShop.id + "']");
            for (var i = 0; i < cards.length; i++) {
                cards[i].classList.add("selected");
            }
            this.drawShops();
        },
        centerOnSelectedShop: function() {
            this.map.setView([this.selectedShop.address.lat, this.selectedShop.address.lng], this.zoom)
        },
        fitBoundsToShops: function() {
            var corners = new Array;
            for (var i = 0; i < this.shopList.length; i++) {
                corners.push(L.latLng(this.shopList[i].address.lat, this.shopList[i].address.lng));
            }
            if (this.from != null) {
                corners.push(L.latLng(this.from[0], this.from[1]));
            }
            var bounds = L.latLngBounds(corners);
            this.map.fitBounds(bounds, {padding: [30, 30]});
        },
        getPopup: function(shop) {
            var opening_state = this.getOpeningState(shop.today_opening_times)
            var popup = '<h6>' + shop.name + '</h6>';
            popup += '<div><i class="icon-marker"></i><p>' + shop.address.address + '</p></div>';
            popup += '<p class="opening">' + opening_state + '</p>';
            var url = Routing.generate('shop_show', { 'slug': shop.slug })
            popup += '<p class="text-center"><a class="btn" href="' + url + '">Page du magasin</a></p>';
            return popup;
        },
        getMinutesFromString: function(timeString) {
            if (typeof timeString !== 'undefined') {
                var hours = parseInt(timeString.split("T")[1].split(':')[0]);
                var minutes = parseInt(timeString.split("T")[1].split(':')[1]);
                return hours * 60 + minutes;
            } else {
                return 'undefined'
            }
        },
        onMove() {
            clearTimeout(this.timerMove);
            var self = this;
            this.timerMove = setTimeout(function() {
                self.zoom = self.map.getZoom();
                self.center = self.map.getCenter();
                self.bounds = self.map.getBounds();
                //--Evite un premier rechargement lors du premier fitBound dans le mounted
                if (self.isRealyInit) {
                    //self.submit('map');
                    self.moved = true;
                }
                self.isRealyInit = true;
            }, 500);
        },
        switchShow(isMap) {
            if (isMap) {
                this.mobileMap = true;
                this.mobileList = false;
                this.zoom = 14
                //--On initialise les valeurs
                this.center = this.map.getCenter();
                this.bounds = this.map.getBounds();
                this.savedBounds = this.bounds;

                //-- On remet le scroll à 0
                document.documentElement.scrollTop = 0

            } else {
                this.mobileMap = false;
                this.mobileList = true;
            }
            this.moved = false;
            this.isRealyInit = false;
        },
        reloadAfterMove() {
            this.savedBounds = this.bounds;
            this.moved = false;
            this.submit('map');
        },
        addFromPosition(lng, lat) {
            this.from = [lat, lng]
            this.addFromIcon()
            this.fitBoundsToShops()
        },
        addFromIcon() {
            if (this.fromMarker != null) {
                this.map.removeLayer(this.fromMarker);
            }
            if (this.from != null) {
                this.fromMarker  = L.marker(this.from, {
                    icon: fromIcon
                });
                this.fromMarker.addTo(this.map)
            }
        },
        nextResults() {
            this.submit('next');
        },
        prevResults() {
            this.submit('prev');
        },
        linkShop: function(slug) {
            return Routing.generate('shop_show', { 'slug': slug })
        },
        signalError: function() {
            var product = this.productShowing;
            var price = 0
            var stock = 0
            if (this.signalPrice) {
                price = 1
            }
            if (this.signalStock) {
                stock = 1
            }
            var description = this.signalDescription;

            if(price == 0 && stock == 0) {
                this.signalWarning = true
            } else {
                axios.get(Routing.generate("signal_product", { 'product': product, 'price': price, 'stock': stock, 'description': description })).then(function (response) {
                    searchProduct.signalProductShowing = false
                    searchProduct.signalDescription = ''
                    searchProduct.signalStock = false
                    searchProduct.signalPrice = false
                    searchProduct.signalSuccess = true
                    searchProduct.signalWarning = false
                })
                .catch(err => console.log(err))
            }
        },
        imageProduct: function(product) {
            if (typeof product.product_image === 'undefined') {
                return "/dist/img/" + product.type.icon + "_default.svg"
            }
            return this.productImagesPath + "/" + product.product_image.image_name;
        },
        updateActiveFilters: function() {
            //--Les filtre sont un tableau : type, valeur, label
            this.activeFilters = new Array;
            if (this.form_what !== '') {
                this.activeFilters.push(['form_what', this.form_what, '<i class="icon-search"></i>' + this.form_what]) //--On désactive pour le moment
            }
            if (this.$refs.formAddressControl.address !== '') {
                this.activeFilters.push(['address', this.$refs.formAddressControl.address, '<i class="icon-geoloc"></i>' + this.$refs.formAddressControl.address])
            }
            var origin = document.querySelector('input[name="product_search[origin]"]:checked');
            if (origin.value !== '4_world') {
                var label = document.querySelector('label[for="' + origin.id + '"]')
                this.activeFilters.push(['origin', origin.value, '<i class="icon-' + origin.value  + '"></i>' + label.innerHTML])
            }

            var labels = document.querySelectorAll('input[name="product_search[labels][]"]:checked');
            for (var i = 0; i < labels.length; i++) {
                var label = document.querySelector('label[for="' + labels[i].id + '"]')
                this.activeFilters.push(['label', labels[i].value, label.innerHTML])
            }

            var usages = document.querySelectorAll('input[name="product_search[usages][]"]:checked');
            for (var i = 0; i < usages.length; i++) {
                var label = document.querySelector('label[for="' + usages[i].id + '"]')
                this.activeFilters.push(['usage', usages[i].value, label.innerHTML])
            }

            var types = document.querySelectorAll('input[name="product_search[types][]"]:checked');
            for (var i = 0; i < types.length; i++) {
                var label = document.querySelector('label[for="' + types[i].id + '"]')
                this.activeFilters.push(['type', types[i].value, label.innerHTML])
            }
        },
        deleteActiveFilter: function(type, value) {
            if (type == 'form_what') {
                this.form_what = "";
                document.getElementById('product_search_search').value = ""
            } else if (type == 'address') {
                this.$refs.formAddressControl.address = ""
                this.$refs.formAddressControl.lat = null
                this.$refs.formAddressControl.lng = null
                document.getElementById('product_search_address').value = ""
                document.getElementById('product_search_lat').value = ""
                document.getElementById('product_search_lng').value = ""
            } else if (type == 'origin') {
                document.getElementById('product_search_origin_0').checked = true
            } else if (type == 'label') {
                document.getElementById("product_search_labels_" + value).checked = false;
            } else if (type == 'usage') {
                document.getElementById("product_search_usages_" + value).checked = false;
            } else if (type == 'type') {
                document.getElementById("product_search_types_" + value).checked = false;
            }
            this.submit('new')
        },
        sort: function(type) {
            if (type === 'distance') {
                this.productList = this.productList.sort(function(a, b) {
                    return a.distance - b.distance;
                })
            } else if (type === 'price') {
                this.productList = this.productList.sort(function(a, b) {
                    return a.price - b.price;
                })
            } else if (type === 'name') {
                this.productList = this.productList.sort(function(a, b) {
                    if (a.name < b.name) {
                        return -1
                    } else if (a.name > b.name) {
                        return 1
                    } else {
                        return 0;
                    }
                })
            }
        },
        getSignalByProduct: function(productIndex, typeSignal) {
            var signal = this.productList[productIndex].signals.filter((signal) => signal.status == 'open' && signal.type == typeSignal)
            if (signal.length > 0)
                return true
            else
                return false
        },
    },
});
