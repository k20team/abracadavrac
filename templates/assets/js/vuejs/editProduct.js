import Vue from 'vue';
import mixinDetictingMobile from './mixins/isMobile';

new Vue({
    delimiters: ['${', '}'],
    el: '#edit-product',
    mixins: [mixinDetictingMobile],
    components: {
    },
    data: {
        popupImageShowing: false,
        productImages: datas.productImages,
        selectedCategory: datas.selectedCategory,
        productImagesPath: datas.productImagesPath,
        searchImage: "",
        selectedImageId: document.getElementById("product_productImage").value
        //selectedImage: document.getElementById("product_productImage").value
    },

    mounted() {
    },
    watch: {
        popupImageShowing: function (val) {
            (val) ? document.body.classList.add('is-locked'): document.body.classList.remove('is-locked')
        },
    },
    methods: {
        imageProduct: function(image) {
            if (image != null) {
                return this.productImagesPath + "/" + image.image_name;
            }
            try {
                return "/dist/img/" + this.productFilteredImages[0].product_type.icon + "_default.svg";
            } catch (error) {
                
            }
        },
        selectImage: function(image) {
            if (image == null) {
                this.selectedImageId = null;
            } else {
                this.selectedImageId = image.id;
            }
            this.popupImageShowing = false;
            //document.getElementById("product_productImage").value = image.id;
        },
        openClosePopup: function() {
            if (this.popupImageShowing) {
                this.popupImageShowing = false
            } else {
                this.popupImageShowing = true
                if (!this.isMobile()) {
                    //--On timeout car si la popup n'est pas encore affichée, le focus n'est pas appliqué
                    setTimeout((function() { document.getElementById("images-search").focus(); }), 500)
                }
            }
        } 
    },
    computed: {
        selectedImage: function() {
            if (this.selectedImageId != null) {
                for (var i = 0; i < this.productImages.length; i++) {
                    if (this.productImages[i].id == parseInt(this.selectedImageId)) {
                        return this.productImages[i];
                    }
                }
            }
            return null
        },
        selectedCategoryName: function() {
            if (this.productFilteredImages.length > 0) {
                return this.productFilteredImages[0].product_type.name;
            }
            return "";
        },
        selectedCategoryIcon: function() {
            if (this.productFilteredImages.length > 0) {
                return "/dist/img/" + this.productFilteredImages[0].product_type.icon + "_default.svg";
            }
            return "";
        },
        productFilteredImages: function() {
            var filter = this.productImages.filter(image => image.product_type.id == this.selectedCategory);
            if (this.searchImage != "") {
                filter = filter.filter(image => image.name.toLowerCase().includes(this.searchImage.toLowerCase()));
            }

            return filter;
        }
    }
});
