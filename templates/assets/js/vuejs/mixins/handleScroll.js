var handleScroll = {
    data: function() {
        return {
            lastScrollPosition: 0,
            scrollTop: true,
            scrollLimit: false,
        }
    },

    mounted() {
        window.addEventListener('scroll', this.handleScroll);
    },

    beforeDestroy() {
        window.removeEventListener('scroll', this.handleScroll)
    },

    methods: {
        handleScroll(event) {
            const currentScrollPosition = window.pageYOffset || document.documentElement.scrollTop
            if (currentScrollPosition < 0) return

            // Stop executing this function if the difference between
            // current scroll position and last scroll position is less than some offset
            if (Math.abs(currentScrollPosition - this.lastScrollPosition) < 60) return

            this.scrollTop = currentScrollPosition < this.lastScrollPosition
            this.lastScrollPosition = currentScrollPosition

            this.scrollLimit = (currentScrollPosition > 300)? true : false
        },
        animateScrollTop() {
            jQuery('html, body').animate({
                // Position of page
                scrollTop: 0
            }, 'slow');
        }
    }
}

export default handleScroll
