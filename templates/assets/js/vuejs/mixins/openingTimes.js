var mixinOpeningTimes = {
    methods: {
        getOpeningState: function (today_opening_times) {
            var morning_start = this.getMinutesFromString(today_opening_times.morning_start_at)
            var morning_end = this.getMinutesFromString(today_opening_times.morning_end_at)
            var afternoon_start = this.getMinutesFromString(today_opening_times.afternoon_start_at)
            var afternoon_end = this.getMinutesFromString(today_opening_times.afternoon_end_at)
            var now = new Date();
            var hours = now.getHours();
            var minutes = now.getMinutes();
            var now = hours * 60 + minutes
            var opening_state = 'Horaires non renseignés';
            //--A terminer, si nos est entre les valeur start et end, on affiche ouvert sinon fermé
            if ((morning_start !== 'undefined' || afternoon_start !== 'undefined') && (morning_end !== 'undefined' || afternoon_end !== 'undefined')) {
                if (now > morning_start && now < morning_end) {
                    var delta = morning_end - now;
                    if (delta < 60 ) {
                        opening_state = '<span class="text-secondary">Ouvert pendant encore ' + delta + ' minutes</span>'
                    } else {
                        opening_state = 'Ouvert'
                    }
                } else if (afternoon_start !== 'undefined' && afternoon_end !== 'undefined' && now > afternoon_start && now < afternoon_end) {
                    var delta = afternoon_end - now;
                    if (delta < 60 ) {
                        opening_state = '<span class="text-secondary">Ouvert pendant encore ' + delta + ' minutes</span>'
                    } else {
                        opening_state = 'Ouvert'
                    }
                } else {
                    opening_state = '<span class="text-danger">Fermé</span>'
                }
            }
            return opening_state
        },
        getMinutesFromString: function(timeString) {
            if (typeof timeString !== 'undefined') {
                var hours = parseInt(timeString.split("T")[1].split(':')[0]);
                var minutes = parseInt(timeString.split("T")[1].split(':')[1]);
                return hours * 60 + minutes;
            } else {
                return 'undefined'
            }
        },
    }
}

export default mixinOpeningTimes
