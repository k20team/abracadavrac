import Vue from 'vue';
import {LMap, LTileLayer, LMarker, LControl} from 'vue2-leaflet';
import { Icon } from "leaflet";

delete Icon.Default.prototype._getIconUrl;

Icon.Default.mergeOptions({
    shadowUrl: '',
    iconRetinaUrl: require('../../../img/map/marker.svg'),
    iconUrl: require('../../../img/map/marker.svg'),
    iconSize: [30, 43],
    iconAnchor: [15, 43]
});
var fromIcon = L.icon({
    shadowUrl: '',
    iconRetinaUrl: require('../../../img/map/marker-user.svg'),
    iconUrl: require('../../../img/map/marker-user.svg'),
    iconSize: [30, 43], // size of the icon
});
var selectedIcon = L.icon({
    shadowUrl: '',
    iconRetinaUrl: require('../../../img/map/marker-selected.svg'),
    iconUrl: require('../../../img/map/marker-selected.svg'),
    iconSize: [30, 43], // size of the icon
    iconAnchor: [15, 43]
});
Vue.component('l-map', LMap);
Vue.component('l-tilelayer', LTileLayer);
Vue.component('l-marker', LMarker);

export {fromIcon, selectedIcon}
