import Vue from 'vue';

var MenuBurger = Vue.extend({
    template: `<button
            @click.prevent="showNavbar"
            class="navbar-toggler"
            type="button">
                <i class="icon-burger"></i>
        </button>`,

    mounted: function () {
        document.getElementById('overlay-nav').addEventListener('click', this.showNavbar);
        document.getElementById('close-nav').addEventListener('click', this.showNavbar);
    },

    beforeDestroy: function () {
        document.getElementById('overlay-nav').removeEventListener('click', this.showNavbar);
        document.getElementById('close-nav').removeEventListener('click', this.showNavbar);
    },

    methods: {
        showNavbar: function() {
            jQuery("body").toggleClass('is-locked');
            jQuery("#header-sidebar").toggleClass('show');
        },
    }
});

new MenuBurger().$mount('#vue-menu-burger');
