import Vue from 'vue';
import addressControl from './components/addressControl';
import mixinDetictingMobile from './mixins/isMobile';
import mixinOpeningTimes from './mixins/openingTimes';
import handleScroll from './mixins/handleScroll';
var VueTruncate = require('./utilities/vue-truncate');
Vue.use(VueTruncate);
Vue.use(require('vue-moment'));

import {
    fromIcon,
    selectedIcon
} from "./utilities/markers-leaflet";

import Slick from 'vue-slick';
import 'slick-carousel/slick/slick.css'

new Vue({
    delimiters: ['${', '}'],
    el: '#search-shop',
    mixins: [mixinDetictingMobile, handleScroll, mixinOpeningTimes],
    components: {
        addressControl,
        Slick
    },
    data: {
        from: datas.from,
        center: datas.center,
        zoom: datas.zoom,
        bounds: null,
        savedBounds: null,
        map: null,

        markersList: null,
        fromMarker: null,

        shopList: datas.shopList,
        selectedShop: null,

        startLimit: datas.startLimit,
        nbrResults: datas.shopList.length,
        nbrResultsTotal: datas.totalNumber,
        showedNumber: datas.showedNumber,
        currentPage: 1,

        moved: false,
        timerMove: null,
        isRealyInit: false,
        searchInProgress: false,

        mobileList: true,
        mobileMap: false,
        filterShowing: '',
        itemShowed: '',
        form_what: datas.what,

        slickOptions: {
            slidesToShow: 1,
            arrows: false,
            infinite: false,
            centerMode: true,
            centerPadding: '20px',
            mobileFirst: true,
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        centerPadding: '40px',
                    }
                }
            ]
        },
    },

    watch: {
        /*filterShowing: function (val) {
            (val === 'show' || val === 'open') ? document.body.classList.add('is-locked') : document.body.classList.remove('is-locked')
        },
        itemShowed: function (val) {
            (val != '') ? document.body.classList.add('is-locked'): document.body.classList.remove('is-locked')
        },*/
        mobileMap: function (val) {
            if (val) {
                this.$nextTick(() => {
                    this.map.invalidateSize(true)
                    this.map.setView([this.center.lat, this.center.lng], this.zoom);
                    this.$refs.slick.reSlick();
                    this.initMapFromSlider()
                });
            }
        },
        shopList: function (val) {
            if (this.mobileMap ) {
                this.$refs.slick.destroy()
                this.selectedShop = null
                this.$nextTick(() => {
                    this.$refs.slick.create()
                    this.initMapFromSlider()
                })
            }
        }
    },
    mounted() {
        this.map = this.$refs.map.mapObject
        this.map.scrollWheelZoom.disable();
        this.addFromIcon();
        this.bounds = this.map.getBounds();
        this.drawShops();
        if (this.shopList.length > 0) {
            this.fitBoundsToShops();
        }
        //this.savedBounds = this.bounds;

        //--Initialisation (seulement en desktop)
        if (!this.isMobile()) {
            this.zoom = this.map.getZoom();
            this.center = this.map.getCenter();
            this.bounds = this.map.getBounds();
            //this.savedBounds = this.bounds;
        }

        this.$refs.slick.$on('afterChange', this.handleAfterChange);
    },

    computed: {
        totalPageNumber: function() {
            return Math.ceil(this.nbrResultsTotal / this.showedNumber)
        }
    },

    methods: {
        // Methode de recherche
        search: function () {
          var self = this;
          self.submit('new', null);
        },
        submit: function (type) {
            document.documentElement.scrollTop = 0
            this.searchInProgress = true;
            this.filterShowing = '';

            var self = this;
            if (type == 'new' || type == 'map') {
                self.startLimit = 0;
                self.currentPage = 1;
            } else if (type == 'next') {
                self.startLimit = self.startLimit + self.showedNumber;
                self.currentPage = self.currentPage + 1;
            } else if (type == 'prev') {
                self.startLimit = self.startLimit - self.showedNumber;
                self.currentPage = self.currentPage - 1;
            }
            var searchForm = $('#search-form');
            var formDatas = searchForm.serialize();

            //--On ajoute l'url à l'historique
            var url = Routing.generate("search_shop")+"?"+formDatas;
            window.history.pushState('', '', url);

            //--On ajoute les données utiles
            formDatas = formDatas + "&startLimit=" + self.startLimit + "&ajax=1";
            //--Si la recherche vient de la map, on borne sur le scope de la map
            if ((type == 'map' || type == 'next' || type == 'prev') && self.savedBounds != null ) {
                if (!self.isMobile() || (self.isMobile() && self.mobileMap)) {
                    formDatas = formDatas + "&minLat=" + self.savedBounds._southWest.lat + "&minLng=" + self.savedBounds._southWest.lng + "&maxLat=" + self.savedBounds._northEast.lat + "&maxLng=" + self.savedBounds._northEast.lng;
                }
            }
            $.get(
                Routing.generate("search_shop"),
                formDatas,
                function(datas) {
                    self.shopList = datas[0];
                    self.drawShops();
                    //--On recadre sur les markers seulement si la recherche ne vient pas d'un movement de la carte
                    self.nbrResults = datas[0].length
                    self.nbrResultsTotal = datas[1]
                    self.searchInProgress = false;

                    if (datas[2] && datas[3]) {
                        self.from = [parseFloat(datas[3]), parseFloat(datas[2])]
                    } else {
                        self.from = null
                    }
                    self.addFromIcon()
                    if (type == 'new') {
                        if (self.shopList.length > 0) {
                            self.fitBoundsToShops();
                        }
                    }
                }
            );
        },
        drawShops: function() {
            if (this.markersList != null) {
                for (var i = 0; i < this.markersList.length; i++) {
                    this.map.removeLayer(this.markersList[i]);
                }
            }
            this.markersList = new Array;
            //--On dessine les markers
            for (var i = 0; i < this.shopList.length; i++) {
               //--le marker sélectionné
                if (this.selectedShop != null && this.shopList[i].id == this.selectedShop.id) {
                    var marker = L.marker(L.latLng(this.shopList[i].address.lat, this.shopList[i].address.lng), {icon: selectedIcon})
                    .addTo(this.map)
                    .bindPopup(this.getPopup(this.shopList[i])).openPopup()
                    .on('click', this.clickOnMapItem.bind(null, this.shopList[i]));
                } else { //--Les autres markers
                    var marker = L.marker(L.latLng(this.shopList[i].address.lat, this.shopList[i].address.lng))
                    .addTo(this.map)
                    .on('click', this.clickOnMapItem.bind(null, this.shopList[i]));
                }
                this.markersList.push(marker);
            }
        },
        initMapFromSlider: function() {
            if (this.selectedShop === null) {
                var currentShop = this.$refs.slick.$el.slick.$slides[0].childNodes[0].childNodes[0].dataset.shop;
                this.selectShopFromSlider(currentShop)
            }
            this.selectShop();
            this.centerOnSelectedShop();
        },
        handleAfterChange: function (event, slick, currentSlide) {
            console.log('change')
            var currentShop = slick.$slides[currentSlide].childNodes[0].childNodes[0].dataset.shop;
            this.selectShopFromSlider(currentShop)
            this.selectShop();
            this.centerOnSelectedShop();
        },
        selectShopFromSlider: function(currentShopId) {
            var shop = null;
            for (var i = 0; i < this.shopList.length; i++) {
                if (this.shopList[i].id == currentShopId) {
                    shop = this.shopList[i]
                }
            }
            this.selectedShop = shop;
        },
        clickOnMapItem: function(shop, e) {
            this.selectedShop = shop;
            this.selectShop();
        },
        mouseOverOnListItem: function (shop) {
            if (this.selectedShop != shop) {
                this.selectedShop = shop;
                this.selectShop();
            }
        },
        clickOnListItem: function(shop) {
            if (this.selectedShop != shop){
                this.selectedShop = shop;
                this.selectShop();
            }
            this.centerOnSelectedShop();
        },
        selectShop: function() {
            var allCards = document.querySelectorAll("[data-shop]");
            for (var i = 0; i < allCards.length; i++) {
                allCards[i].classList.remove("selected");
            }
            var cards = document.querySelectorAll("[data-shop='" + this.selectedShop.id + "']");
            for (var i = 0; i < cards.length; i++) {
                cards[i].classList.add("selected");
            }
            this.drawShops();
        },
        centerOnSelectedShop: function() {
            this.map.setView([this.selectedShop.address.lat, this.selectedShop.address.lng], this.zoom)
        },
        fitBoundsToShops: function() {
            var corners = new Array;
            for (var i = 0; i < this.shopList.length; i++) {
                corners.push(L.latLng(this.shopList[i].address.lat, this.shopList[i].address.lng));
            }
            if (this.from != null) {
                corners.push(L.latLng(this.from[0], this.from[1]));
            }
            var bounds = L.latLngBounds(corners);
            this.map.fitBounds(bounds, {padding: [30, 30]});
        },
        getPopup: function(shop) {
            console.log(shop)
            var opening_state = this.getOpeningState(shop.today_opening_times)
            var popup = '<h6>' + shop.name + '</h6>';
            popup += '<div><i class="icon-marker"></i><p>' + shop.address.address + '</p></div>';
            popup += '<p>' + opening_state + '</p>';
            var url = Routing.generate('shop_show', { 'slug': shop.slug })
            popup += '<p class="text-center"><a class="btn" href="' + url + '">Page du magasin</a></p>';
            return popup;
        },
        onMove() {
            clearTimeout(this.timerMove);
            var self = this;
            this.timerMove = setTimeout(function() {
                self.zoom = self.map.getZoom();
                self.center = self.map.getCenter();
                self.bounds = self.map.getBounds();
                //--Evite un premier rechargement lors du premier fitBound dans le mounted
                if (self.isRealyInit) {
                    //self.submit('map');
                    self.moved = true;
                }
                self.isRealyInit = true;
            }, 500);
        },
        switchShow(isMap) {
            if (isMap) {
                this.mobileMap = true;
                this.mobileList = false;
                this.zoom = 14
                //--On initialise les valeurs
                this.center = this.map.getCenter();
                this.bounds = this.map.getBounds();
                this.savedBounds = this.bounds;

                //-- On remet le scroll à 0
                document.documentElement.scrollTop = 0
            } else {
                this.mobileMap = false;
                this.mobileList = true;
            }
            this.moved = false;
            this.isRealyInit = false;
        },
        reloadAfterMove() {
            this.savedBounds = this.bounds;
            this.moved = false;
            this.submit('map');
        },
        addFromPosition(lng, lat) {
            this.from = [lat, lng]
            this.addFromIcon()
            this.fitBoundsToShops()
        },
        addFromIcon() {
            if (this.fromMarker != null) {
                this.map.removeLayer(this.fromMarker);
            }
            if (this.from != null) {
                this.fromMarker  = L.marker(this.from, {
                    icon: fromIcon
                });
                this.fromMarker.addTo(this.map)
            }
        },
        nextResults() {
            this.submit('next');
        },
        prevResults() {
            this.submit('prev');
        },
        linkShop: function(slug) {
            location.href = Routing.generate('shop_show', {
                'slug': slug
            })
        },
        sort: function(type) {
            console.log(this.shopList)
            if (type === 'distance') {
                this.shopList = this.shopList.sort(function(a, b) {
                    return a.distance - b.distance;
                })
            } else if (type === 'name') {
                this.shopList = this.shopList.sort(function(a, b) {
                    if (a.name < b.name) {
                        return -1
                    } else if (a.name > b.name) {
                        return 1
                    } else {
                        return 0;
                    }
                })
            }
        }
    }
});
