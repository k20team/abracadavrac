import Vue from 'vue';
import axios from 'axios';

const addressControl = Vue.component('address-control', {
    delimiters: ['${', '}'],
    template: ``,
    props: {
        address_prop: {
            type: String,
            required: false
        },
        lng_prop: {
            type: Number,
            required: false
        },
        lat_prop: {
            type: Number,
            required: false
        }
    },

    data() {
        return {
            timer: null,
            address: this.address_prop?this.address_prop:"",
            lng: this.lng_prop?this.lng_prop:null,
            lat: this.lat_prop?this.lat_prop:null,
            autocompleteAdresses: [],
        }
    },
    mounted() {
    },
    watch: {
    },

    methods: {
        writeAddress: function () {
            var self = this;
            this.lng = null;
            this.lat = null;
            window.clearTimeout(self.timer);
            self.timer = setTimeout(function() {
                self.autocomplete();
            }, 500);
        },
        autocomplete: function() {
            var self = this;
            var url = Routing.generate('search_autocomplete_address', {
                'address': self.address
            });
            axios.post(url)
            .then(response => {
                var values = response.data;
                self.autocompleteAdresses = values;
            })
            .catch(e => {
                console.log(e);
            })
        },
        selectAdress: function(item) {
            this.address = item.address;
            this.lng = item.lng;
            this.lat = item.lat;
        },
        getUserLocation: function() {
            var self = this;

            navigator.geolocation.getCurrentPosition(function (position) {
                self.lng = position.coords.longitude;
                self.lat = position.coords.latitude;

                if (typeof self.$parent.addFromPosition !== "undefined") {
                    self.$parent.addFromPosition(self.lng, self.lat)
                }

                var url = Routing.generate('search_address_from_location', {
                    'lng': self.lng,
                    'lat': self.lat
                });
                axios.post(url)
                .then(response => {
                    self.address = response.data.address
                })
                .catch(e => {
                    console.log(e);
                })
            });
        },
        hideAutocomplete: function (control) {
            var parentAddressControl = control.target.parentNode.parentNode
            setTimeout(function () {
                parentAddressControl.getElementsByClassName('autocomplete-address')[0].classList.add('hide')
            }, 200)
        },
        showAutocomplete: function (control) {
            var parentAddressControl = control.target.parentNode.parentNode
            parentAddressControl.getElementsByClassName('autocomplete-address')[0].classList.remove('hide')
        },
    },
})

export default addressControl
