import Vue from 'vue';
import Slick from 'vue-slick';
import 'slick-carousel/slick/slick.css'
Vue.use(require('./utilities/vue-truncate'));
Vue.use(require('vue-moment'));

new Vue({
    delimiters: ['${', '}'],
    el: '#collector-home',
    components: {
        Slick
    },
    data: {
        products: datas.products,
        shops: datas.shops,
        productImagesPath: datas.productImagesPath,
        slickOptions: {
            slidesToShow: 4,
            infinite: false,
            responsive: [{
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        },
    },

    mounted() {
    },
    methods: {
        /*next() {
            this.$refs.slick.next();
        },*/
        linkShop(shop) {
            return Routing.generate("shop_show", {'slug':shop.slug });
        },
        linkAddProduct(shop) {
            return Routing.generate("create_product", {'slug':shop.slug })
        },
        linkEditProduct(product) {
            return Routing.generate("edit_product", {
                'product': product.id
            })
        },
        imageProduct: function(product) {
            if (typeof product.product_image === 'undefined') {
                return "/dist/img/" + product.type.icon + "_default.svg"
            }
            return this.productImagesPath + "/" + product.product_image.image_name;
        },
    }
});
